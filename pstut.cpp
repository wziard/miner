#include "globals.h"
#include "gfxdata.h"
#include "pstut.h"
#include "objidx.h"

PSTUT::PSTUT()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_PSTUT].dat);
   tag = TAG_MONSTER;
   passflags |= PF_PLAYER;
   score.poorts = 3;
   name ="PSTUT";
   idx = OBJ_PSTUT;
}

int PSTUT::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


