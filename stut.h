#ifndef _STUT_H_
#define _STUT_H_
#include "objects.h"

class STUT
    : public STATIC_OBJECT
{
   public:
      STUT();
      int proc();
      int can_coexist(int tag);
};


#endif
