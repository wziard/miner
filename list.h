#ifndef _LIST_H_
#define _LIST_H_
#include <allegro.h>

#define F_CONTINUE 0
#define F_DONE 1
#define F_DELETE 2


#define PF_NONE_SHALL_PASS  0
#define PF_PLAYER (1 << 0)
#define PF_AXER   (1 << 1)
#define PF_MONSTER  (1 << 2)
#define PF_SMARTMONSTER (1 << 3)
#define PF_STRUCTURE     (1 << 4)
#define PF_ALL (PF_PLAYER | PF_AXER | PF_MONSTER | PF_SMARTMONSTER | PF_STRUCTURE)


#define TAG_BACKGROUND  0 // not influnced by anything
#define TAG_STRUCTURE 1 // can coexist with player and smartmonsters like background, but not with rest
#define TAG_MONSTER 2// cannot coexist with anything except background & structures
#define TAG_PLAYER 3//cannot coexist with anything except background & structures
#define TAG_AXER 4 //cannot coexist with anything except background & structures
#define TAG_BONUS 5
#define TAG_SMARTMONSTER 6
#define TAG_STRONGSTRUCTURE 7 // can coexist with player and smartmonsters like background, but no background, cannot be blown up
#define TAG_PLACEHOLDER 8// special case


class Score
{

   public:
          Score();
          short energy;
          int points;
          char lives;
          short stars;
          short poorts;
          char houws;
          inline friend Score operator +(Score one, Score other)
          {
               Score r;
               r. energy = one.energy + other.energy;
               r. lives = one.lives + other.lives;
               r. points = one.points + other.points;
               r. stars = one.stars + other.stars;
               r. poorts = one.poorts + other.poorts;
               r. houws = one.houws + other.houws;
               return r;
          }
          Score &operator = ( const Score &other)
          {
               energy = other.energy;
               lives = other.lives;
               stars = other.stars;
               houws = other.houws;
               poorts = other.poorts;
               points = other.points;
               return *this;
          }
          Score &operator += (const Score &other)
          {
               energy += other.energy;
               lives += other.lives;
               poorts += other.poorts;
               houws += other.houws;
               stars += other.stars;
               points += other.points;
               return *this;
          }

};


typedef struct POSITION
{
 int x,y;
} POSITION;

class OBJECT
{
   public:
     friend class FUNCTIONLIST;
     friend class OBJECT_COLLECTION;
     
     OBJECT();
     virtual ~OBJECT();
     virtual int proc() = 0; // returns 0 if finished
     virtual void draw(BITMAP *onto,int x, int y, int layer) = 0; // x & y are the position ont he bitmap to draw
     OBJECT *get_next();
     void add_to_list(OBJECT *head);
     void remove_from_list();
     unsigned short int mapx, mapy;
     char xoffs, yoffs;
     unsigned short passflags; //bitflags who may pass (0 - none shall pass, 1 only player etc)
     int tag; //type of object
     Score score;
     virtual int can_coexist(int other_tag);

     char nodelete; // don't ever really delete, just remove from map


     virtual void die(); // defualt this sets must_be_deleted to true, can be overloaded to don something else for things with more lives (player)
     char must_be_deleted; // can be used to mark an object for deletion
     char const *name;

     short idx; // this object's personal index in the collector's array

     virtual void set_state(int state, int info);
     
   private:
     OBJECT *last;
     OBJECT *next;
     
   protected:
     Score move(int dx, int dy);
     char state;
     short frame;
     static short id_count;
     short id;
     short voice;
};

class HEAD
  :  public OBJECT
{
   public:
      HEAD();
      ~HEAD();
      int proc();
      void draw(BITMAP *onto, int x, int y, int l);
};




/*
   functionlist
   contains a linked list of objects
   it calls all objects's proc functions
   if a proc function returns 1 this means the object is
   ready with it's task, and it does not need to be called anymore
   it will then be thrown out of the list
 */

class FUNCTIONLIST
{
   public:
     FUNCTIONLIST();
     ~FUNCTIONLIST();
     void do_procs();
     void add(OBJECT *obj);
   private:
     OBJECT *head;
     OBJECT *waitroom; // will be added to head next round
};

/* drawlist contains pointers to all objects that need to be drawn,
   since it's refilled from the ground up for each frame
   it is implemented with a simple array
   (there is no need to remove elements from the middle)
 */
class DRAWLIST
{
   public:
   
     DRAWLIST(int size);
     ~DRAWLIST();
     void add(OBJECT *object);
     void draw(BITMAP *onto, int layer);
     void for_all(void (*proc)(int x,int y));
     void reset();
   private:
     void lowlevel_add(int x, int y);
     POSITION *the_list;
     int nr_of_items;
     int size;
};





#endif

