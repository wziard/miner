#include "man.h"
#include "stdio.h"
#include "stut.h"
#include "interfac.h"
#include "gfxdata.h"
#include "objidx.h"

MAN::MAN(int _nr_of_lives)
          : COMPLEX_OBJECT()
{

   if (!(gfx_objects_table->gfx_objects[OBJ_MAN]))
   {
      gfx_objects_table->init_object(OBJ_MAN, 15);
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_STAND,(BITMAP *)(anims[GFXDATA_ANIMS_MANS].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_WALKR,(BITMAP *)(anims[GFXDATA_ANIMS_MANR].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_WALKL,(BITMAP *)(anims[GFXDATA_ANIMS_MANL].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_WALKU,(BITMAP *)(anims[GFXDATA_ANIMS_MANO].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_WALKD,(BITMAP *)(anims[GFXDATA_ANIMS_MANN].dat));
   
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HAK,(BITMAP *)(anims[GFXDATA_ANIMS_MANHAK].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HAKR,(BITMAP *)(anims[GFXDATA_ANIMS_HAKR].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HAKL,(BITMAP *)(anims[GFXDATA_ANIMS_HAKL].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HAKU,(BITMAP *)(anims[GFXDATA_ANIMS_HAKO].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HAKD,(BITMAP *)(anims[GFXDATA_ANIMS_HAKN].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_HOERA,(BITMAP *)(anims[GFXDATA_ANIMS_HOERA].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_OH,(BITMAP *)(anims[GFXDATA_ANIMS_MANOH].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_BOIL,(BITMAP *)(anims[GFXDATA_ANIMS_BOIL].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_MELT,(BITMAP *)(anims[GFXDATA_ANIMS_MANZINK].dat));
      gfx_objects_table->set_object_state(OBJ_MAN,MAN_STATE_TIL,(BITMAP *)(anims[GFXDATA_ANIMS_MANT].dat));
   }

   gfx_object = gfx_objects_table->gfx_objects[OBJ_MAN];
   my_score.lives = _nr_of_lives;
   my_score.houws = 1;
   passflags = PF_PLAYER;
   tag = TAG_PLAYER;
   name = "MAN";
   my_layer = 2;
   stars_to_get = 5;
   idx = OBJ_MAN;
}


MAN::~MAN()
{
}


int MAN::proc()
{
   frame++;
   frame %=32;

   switch(state)
   {
      case MAN_STATE_BOIL:
          xoffs = yoffs = 0;
          tag = TAG_BACKGROUND; // we're just decor now
          if (keypressed())
          {
            clear_keybuf();
            state =MAN_STATE_STAND;
            playfield->del(mapx,mapy,this);
            playfield->set(2,2,this);
            playfield->center(2,2);
            my_score.lives--;
            tag = TAG_PLAYER;
          }
         break;
      case MAN_STATE_HAKR:
         if (frame ==  1)
            try_sample(SNDDATA_KLOING, mapx, mapy);
      
         if (frame == 31)
         {
            move(1,0);

            if (!(key[KEY_RIGHT] && (key[KEY_RCONTROL] || key[KEY_LCONTROL])) || !playfield->allowed_move(mapx+1,mapy,PF_AXER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            
         }
         else
         {
            xoffs++;
         }
         break;

      case MAN_STATE_HAKD:
         if (frame ==  1)
            try_sample(SNDDATA_KLOING, mapx,mapy);

         if (frame == 31)
         {
            move(0,1);
            if (!(key[KEY_DOWN] && (key[KEY_RCONTROL] || key[KEY_LCONTROL])) || !playfield->allowed_move(mapx,mapy+1,PF_AXER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            yoffs++;
         }
         break;

      case MAN_STATE_HAKL:
         if (frame ==  1)
            try_sample(SNDDATA_KLOING, mapx,mapy);
         if (frame == 31)
         {
            move(-1,0);
            if (!(key[KEY_LEFT] && (key[KEY_RCONTROL] || key[KEY_LCONTROL])) || !playfield->allowed_move(mapx-1,mapy,PF_AXER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            xoffs--;
         }
         break;

      case MAN_STATE_HAKU:
         if (frame ==  1)
            try_sample(SNDDATA_KLOING, mapx,mapy);
         if (frame == 31)
         {
            move(0,-1);
            if (!(key[KEY_UP] && (key[KEY_RCONTROL] || key[KEY_LCONTROL])) || !playfield->allowed_move(mapx,mapy-1,PF_AXER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            yoffs--;
         }
         break;


      case MAN_STATE_WALKR:
         if (frame == 31)
         {
            move(1,0);
            if (!key[KEY_RIGHT] || !playfield->allowed_move(mapx+1,mapy,PF_PLAYER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            xoffs++;
         }
         break;

      case MAN_STATE_WALKD:
         if (frame == 31)
         {
            move(0,1);
            if (!key[KEY_DOWN] || !playfield->allowed_move(mapx,mapy+1,PF_PLAYER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            yoffs++;
         }
         break;

      case MAN_STATE_WALKL:
         if (frame == 31)
         {
            move(-1,0);
            if (!key[KEY_LEFT] || !playfield->allowed_move(mapx-1,mapy,PF_PLAYER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            xoffs--;
         }
         break;

      case MAN_STATE_WALKU:
         if (frame == 31)
         {
            move(0,-1);
            if (!key[KEY_UP] || !playfield->allowed_move(mapx,mapy-1,PF_PLAYER))
               state = MAN_STATE_STAND;
            else
               playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
         }
         else
         {
            yoffs--;
         }
         break;
      case MAN_STATE_MELT:
         if (frame == 31)
          {
            clear_keybuf();
            state =MAN_STATE_STAND;
            playfield->del(mapx,mapy,this);
            playfield->set(2,2,this);
            playfield->center(2,2);
            my_score.lives--;
            my_score.energy = 800;
            tag = TAG_PLAYER;
          }

         break;
      default:
         if (state == MAN_STATE_HAK && !(key[KEY_RCONTROL] || key[KEY_LCONTROL]))
            state = MAN_STATE_STAND;
            
      // fall through intended
      case MAN_STATE_TIL:
         if (state == MAN_STATE_TIL && frame == 31) // if necessary because of fall-through
               my_score.energy--;
      // fall through intended
      case MAN_STATE_STAND:
      if (playfield->contains_object(mapx, mapy - 1, OBJ_BOX) ||
          playfield->contains_object(mapx, mapy - 1, OBJ_MINE) ||
          playfield->contains_object(mapx, mapy - 1, OBJ_NE_RE) ||
          playfield->contains_object(mapx, mapy - 1, OBJ_NE_LI)
         )
      {
         state = MAN_STATE_TIL;
      }
      else if (state == MAN_STATE_TIL)
      {
         state = MAN_STATE_STAND;
      }

      if (key[KEY_ENTER])
      {
         if (my_score.poorts >0)
         {
            if (!playfield->contains_type(mapx,mapy,TAG_STRONGSTRUCTURE))
            {
               playfield->set(mapx,mapy,objcol->new_obj(OBJ_STUT));
               my_score.poorts--;
            }
         }
      }
      if (!(key[KEY_RCONTROL] || key[KEY_LCONTROL]) && !key[KEY_PLUS_PAD])
      {
           if (key[KEY_LEFT] && playfield->allowed_move(mapx-1,mapy, PF_PLAYER))
           {
               state = MAN_STATE_WALKL;
               playfield->set(mapx-1,mapy, objcol->new_obj(OBJ_PLACEHOLDER));
               frame = 0;
           }
           else if (key[KEY_RIGHT] && playfield->allowed_move(mapx+1,mapy, PF_PLAYER))
           {
               state = MAN_STATE_WALKR;
               playfield->set(mapx+1,mapy, objcol->new_obj(OBJ_PLACEHOLDER));
               frame = 0;
           }
           else if (key[KEY_UP] && playfield->allowed_move(mapx,mapy-1, PF_PLAYER))
           {
               state = MAN_STATE_WALKU;
               playfield->set(mapx,mapy-1, objcol->new_obj(OBJ_PLACEHOLDER));
               frame = 0;
               
           }
           else if (key[KEY_DOWN] && playfield->allowed_move(mapx,mapy + 1, PF_PLAYER))
           {
               state = MAN_STATE_WALKD;
               playfield->set(mapx,mapy+1, objcol->new_obj(OBJ_PLACEHOLDER));
               frame = 0;
           }
      }
      else if (my_score.houws >0)
      {
           state = MAN_STATE_HAK;
           if (key[KEY_LEFT]  && playfield->allowed_move(mapx-1,mapy,PF_AXER))
           {
               state = MAN_STATE_HAKL;
               playfield->set(mapx-1,mapy, objcol->new_obj(OBJ_PLACEHOLDER));
               frame = 0;
               my_score.houws--;
           }
           else if (key[KEY_RIGHT] && playfield->allowed_move(mapx+1,mapy,PF_AXER))
           {
               state = MAN_STATE_HAKR;
               playfield->set(mapx+1,mapy, objcol->new_obj(OBJ_PLACEHOLDER));
               my_score.houws--;
               frame = 0;
           }
           else if (key[KEY_UP]  && playfield->allowed_move(mapx,mapy-1,PF_AXER))
           {
               state = MAN_STATE_HAKU;
               playfield->set(mapx,mapy-1, objcol->new_obj(OBJ_PLACEHOLDER));
               my_score.houws--;
               frame = 0;
           }
           else if (key[KEY_DOWN] && playfield->allowed_move(mapx,mapy+1,PF_AXER))
           {
               state = MAN_STATE_HAKD;
               playfield->set(mapx,mapy-1, objcol->new_obj(OBJ_PLACEHOLDER));
               my_score.houws--;
               frame = 0;
           }
      }
      break;
   }

   if (my_score.stars >= stars_to_get)
   {
      my_score.stars = 0;
      my_score.lives++;
      stars_to_get++;
   }
   if (my_score.energy <= 0 && state != MAN_STATE_MELT)
   {
      state= MAN_STATE_MELT;
      frame = 0;
   }
//   set_oneplayer_statusbar(my_score.poorts, my_score.houws, my_score.lives, stars_to_get - my_score.stars, my_score.energy,my_score.points);
   points1->turn(my_score.points);
   stars1->turn(stars_to_get -my_score.stars);
   stuts1->turn(my_score.poorts);
   lives1->turn(my_score.lives -1);
   houws1->turn(my_score.houws);
   energ1->turn(my_score.energy);
   
   return F_CONTINUE;
}

void MAN::set_state(int _state, int lives)
{
   state = _state;
   my_score.lives = lives;
   my_score.houws = 1;
   my_score.poorts = 0;
   my_score.stars = 0;
   my_score.points = 0;
   my_score.energy = 800;
   passflags = PF_PLAYER;
   stars_to_get = 5;

}

void MAN::move(int dx, int dy)
{
   my_score += OBJECT::move(dx,dy);
   my_score.energy--;
   if (
      mapx - playfield->vp_x < 3
      || playfield->vp_x + playfield->vp_w - mapx < 4
      )
   {
      playfield->center(mapx,-1); // center in x dir
   }
   if (
       mapy - playfield->vp_y < 3
       || playfield->vp_y + playfield->vp_h - mapy < 4
      )
   {
      playfield->center(-1,mapy);
   }
}
int MAN::gameover()
{
   if (my_score.lives <=0)
      return TRUE;
      
   return FALSE;
}

void MAN::die()
{
   state = MAN_STATE_BOIL;
   clear_keybuf();

}
