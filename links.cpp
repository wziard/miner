#include "globals.h"
#include "gfxdata.h"
#include "links.h"
#include "objidx.h"

LINKS::LINKS()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_LINKS].dat);   
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = LINKS_STATE_STANDING;
   speed = 1;
   name = "LINKS";
   idx = OBJ_LINKS;
}

int LINKS::proc()
{
   OBJECT *collide_monster;
   frame++;
   frame %= (32 / speed);
   switch(state)
   {

      case LINKS_STATE_FALLING:
        try_loop(&voice,SNDDATA_GGGG, mapx, mapy,100,10, 125 * (speed+ 8));
      
        if (frame == (32 / speed) - 1)
        {
            move(-1,0);

            if (!playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
            {

               collide_monster = playfield->contains_type(mapx-1,mapy, TAG_SMARTMONSTER);
               if (collide_monster && (speed >=4))
                     collide_monster->die();
            
               kill_loop(&voice);
               state = LINKS_STATE_STANDING;
            }
            else
            {
               speed = MIN(speed<<1, 8);
               playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            xoffs-=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        if (playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
        {
            state = LINKS_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
