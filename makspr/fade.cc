#include <allegro.h>




int main()
{
   allegro_init();
   set_gfx_mode(GFX_AUTODETECT,320,200,0,0);
   BITMAP *tmp;
   BITMAP *vuur;
   int col,r,b,g;
   vuur=load_bitmap("brand.pcx",desktop_palette);
   set_palette (desktop_palette);
   tmp=create_bitmap(32*32,32);
   for(int i=0;i<32;i++)
   {
      for(int x=0;x<32;x++)
      {
         for(int y=0;y<32;y++)
         {

           col=getpixel(vuur,7*32+x,y);

           if (col)
           {
            r=desktop_palette[col].r;
            g=desktop_palette[col].g;
            b=desktop_palette[col].b;
            r=r*(32-32 *(i/32.0)*(i/32.0))/32;
            g=g*(32-32 *(i/32.0)*(i/32.0))/32 ;
            b=b*(32-32 *(i/32.0)*(i/32.0))/32;
            col=makecol(255*r/63,255*g/63,255*b/63);
           }

           putpixel (tmp,i*32+x,y,col);
         }
      }
   }
   save_bitmap ("gloei.bmp",tmp,desktop_palette);

}




