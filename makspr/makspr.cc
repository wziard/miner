#include "allegro.h"
#include "stdlib.h"








BITMAP *pictures = NULL;
BITMAP *buffer;

int nr_of_images = 0;
int current_color = 0;



void load_images(char *name)
{
   if (exists(name))
   {
       pictures= load_bitmap(name,desktop_palette);
   }
   else
       exit(1);

   
   set_palette(desktop_palette);

}



void draw_grid()
{

   for (int x=0;x<32;x++)
   {
      for (int y=0;y<32;y++)
      {
         rect(buffer,x * 14 , y * 14 , x * 14 + 14, y * 14 + 14,makecol(155,155,155));
      }
   }
}


void draw_colors()
{


   for (int x= 0 ;x<16;x++)
   {
      for(int y = 0;y<16;y++)
      {
         rectfill(buffer,450 + x * 10,y*10,450+x*10+10,y*10+10, x + 16 * y);
         rect(buffer,450 + x * 10,y*10,450+x*10+10,y*10+10, 255 - x + 16 * y);
         if (x + y * 16 == current_color)
         {
          circlefill(buffer,450+x*10+5, y*10+5,3,0);
         }
      }
   }

   textprintf(buffer,font, 500,300, makecol(255,255,255), "%d",current_color);
}

void draw_frame(int frame, int mode = 0)
{
   if (frame <0 || frame >=nr_of_images)
      return;
   
   for (int x=0;x<32;x++)
   {
      for (int y=0;y<32;y++)
      {
       if(mode)
       {
         rect(buffer,x * 14 + 1, y * 14 + 1, x * 14 + 13, y * 14 + 13,getpixel(pictures, x + 32 * frame, y));
       }
       else
       {
         rectfill(buffer,x * 14 + 2, y * 14 + 2, x * 14 + 12, y * 14 + 12,getpixel(pictures, x + 32 * frame, y));
       }
      }
   }
   if (!mode)
      blit(pictures,buffer,frame*32,0,600,440,32,32);
   
   textprintf(buffer,font, 500,320, makecol(255,255,255), "%d",frame);

}




int main(int argc, char **argv)
{
   if (argc <2)
      exit(1);
   int current_frame =0;
   int changed = -1;
   int x,y;


   allegro_init();
   set_gfx_mode(GFX_AUTODETECT, 640,480,0,0);
   buffer = create_bitmap(640,480);
   clear(buffer);
   install_timer();
   install_mouse();
   install_keyboard();
   draw_colors();
   show_mouse(screen);
   load_images(argv[1]);
   nr_of_images = pictures->w  / 32;
   
   draw_grid();

   while(!key[KEY_ESC])
   {

    if (mouse_b & 1)
    {
      while(mouse_b&1);
      x=mouse_x/14;
      y=mouse_y/14;
      
      if (x < 32 && y < 32)
      {
         putpixel(pictures, current_frame*32 +x , y,current_color);
         changed = -1;
      }
      else
      {
         x = mouse_x;
         y = mouse_y;
         if (x > 450 && x < 610 && y < 160)
         {
            x = (x - 450) / 10;
            y = (y) / 10;

            current_color = x + 16*y;
            changed = -1;
         }
      }
    }

    if (mouse_b & 2)
    {
      while(mouse_b&2);
      x=mouse_x/14;
      y=mouse_y/14;
      
      if (x < 32 && y < 32)
      {
         current_color = getpixel(pictures, current_frame*32 +x , y);
         changed = -1;
      }
    }


    if (key[KEY_RIGHT])
    {
      while(key[KEY_RIGHT]);
      current_frame++;
      changed = -1;
    }
    if (key[KEY_LEFT])
    {
      while(key[KEY_LEFT]);
      current_frame--;
      changed = -1;
    }
    if (key[KEY_C])
    {
      while(key[KEY_C]);
      blit(pictures,pictures, (current_frame -1) * 32, 0,current_frame*32,0,32,32);
      changed = -1;
    }
    
    if (changed)
    {
      current_frame = MID(0,current_frame,nr_of_images);

      clear(buffer);
      draw_grid();
      draw_frame(current_frame-1,TRUE);
      draw_frame(current_frame);
      draw_colors();
      show_mouse(NULL);
      blit(buffer,screen,0,0,0,0,640,480);
      show_mouse(screen);
      changed = 0;
    }
   }

   save_bitmap(argv[1], pictures,desktop_palette);

   destroy_bitmap(buffer);
   destroy_bitmap(pictures);

   return 0;
}
END_OF_MAIN();



