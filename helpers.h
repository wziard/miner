#ifndef _helpers_h_
#define _helpers_h_

extern void out_of_mem(char const *msg = NULL);
extern void printerror(char const *format, ...);
extern void warn (char const *format, ...);


#endif
