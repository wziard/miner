#include <stdio.h>
#include <signal.h>
#include "init.h"
#include "loaddata.h"
#include "globals.h"
#include "helpers.h"
#include "fillmap.h"
#include "gfxdata.h"
#include "snddata.h"
#include "data.h"
#include "objcol.h"
#include "effect.h"
#include "snd.h"
//#include <jgmod.h> lin!
//#include <new.h>
#include "objidx.h"
//#include <vdrv.h> lin!

#define WAITTIME 100

static void global_clock()
{

   global_speed_regulator++;
}
END_OF_FUNCTION(global_clock);

// static void my_new_handler()
// {
//    allegro_exit();
//    printf("out of memory in call to new\n");
//    exit(1);
// }
// END_OF_FUNCTION(my_new_handler);


static PALETTE text_palette;
static int text_fade;


void init()
{
   int textpos = 0;
   short voice=-1;
   int nr_voices;




   allegro_init();


   

      
   LOCK_FUNCTION(my_new_handler);
//   set_new_handler(my_new_handler);

   tight_mem = 0; //lin!(_go32_dpmi_remaining_physical_memory() < 5000000);

   set_config_file("miner.cfg");

   
   fade_speed = get_config_int("MINER","FADE_SPEED",0);
   text_fade = get_config_int("MINER","TEXT_FADE",0);
   int scale = get_config_int("MINER", "SCALE", 1);

   if (scale < 1)
   {
    scale = 1;
   }

      
   if (text_fade)
      get_palette(text_palette);
   
   if (text_fade)
      fade_out(4);

   if (desktop_color_depth())
      set_color_depth(desktop_color_depth());

   if (get_config_int(NULL, "gfx_depth",0))
      set_color_depth(get_config_int(NULL, "gfx_depth",0));
   

   int fullscreen = get_config_int("MINER", "FULLSCREEN", 0);
   int screenw = get_config_int("MINER", "SCREENW", 800);
   int screenh = get_config_int("MINER", "SCREENH", 600);
   if (set_gfx_mode(fullscreen ? GFX_AUTODETECT : GFX_AUTODETECT_WINDOWED,screenw,screenh,0,0))
      printerror("Error entering GFX mode");


   load_gfx_data();
   game_palette = (RGB *)(gfx_data[GFXDATA_PAL].dat);
   
   for (int i=0;i<256;i++)
   {
      grayscale_palette[i].r = game_palette[i].r + game_palette[i].g + game_palette[i].b;
      grayscale_palette[i].r = grayscale_palette[i].g = grayscale_palette[i].b = (63 - (grayscale_palette[i].r/6))/3;
   }
   grayscale_palette[0].r = grayscale_palette[0].g  = grayscale_palette[0].b =0;
   
      
   init_effect_tables(SCREEN_W, SCREEN_H);
      
      
   screen_buffer = create_bitmap(SCREEN_W/ scale, (SCREEN_H-96)/scale);
   if (!screen_buffer)
      printerror("out of memory setting up graphics buffer");

   set_palette(black_palette);
   stretch_blit (title_back,screen,0,0,640,480, 0, 0, SCREEN_W, SCREEN_H);
      
   if (fade_speed)
      fade_in(grayscale_palette,fade_speed);
   else
      set_palette(grayscale_palette);
       


//   text_mode(-1);

   textprintf_ex(screen,fnt_sml,5,textpos, makecol(0,0,0), -1,"Used %dx%d %d color mode", SCREEN_W, SCREEN_H, (1 << bitmap_color_depth(screen)) - 1);
   textpos+= text_height(fnt_sml);

   textout_ex(screen,fnt_sml,"Installing timer handler...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   if (install_timer())
      printerror("error installing timer");

   rest(WAITTIME);


   textout_ex(screen,fnt_sml,"Installing keyboard handler...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   rest(WAITTIME);
   if (install_keyboard())
      printerror("error installing keyboard");

   textout_ex(screen,fnt_sml,"Installing joystick...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   rest(WAITTIME);

   LOCK_FUNCTION(global_clock);
   BPS = get_config_int("MINER","BPS",64);
   install_int_ex(global_clock, BPS_TO_TIMER(BPS));
   
//   install_mouse();

   textout_ex(screen, fnt_sml,"Initialising engine...", 5, textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   rest(WAITTIME);
   
   gfx_objects_table = new GFX_OBJECTS_TABLE(NR_OF_OBJECTS);
   proclist = new FUNCTIONLIST();
   drawlist = new DRAWLIST(10000);
   objcol = new OBJECT_COLLECTION;


   textout_ex(screen, fnt_sml,"Initialising sound...", 5, textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   nr_voices = get_config_int("MINER","NR_OF_VOICES",8);
   rest(WAITTIME);

   reserve_voices(nr_voices,0);
   if (install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT, NULL))
   {
//      textout(screen, fnt_sml,"Could not install sound: silence", 15, textpos, makecol(0,0,0));
      textprintf_ex(screen, font, 15, textpos, makecol(0,0,0), -1, "Could not install sound: %s", allegro_error);
      install_sound(DIGI_NONE,MIDI_NONE, NULL);
      textpos+= text_height(fnt_sml);
   }
   else
   {
      textout_ex(screen, fnt_sml,"OK", 15, textpos, makecol(0,0,0), -1);
      textpos+= text_height(fnt_sml);
   }

   if (digi_driver->id != DIGI_NONE)
   {
      textout_ex(screen, fnt_sml,"Loading sound data", 15, textpos, makecol(0,0,0), -1);
      textpos+= text_height(fnt_sml);
      load_snd_data();
   }
   
//   install_mod (8);
//   JGMOD *mod = load_mod("bulg1.xm");

//   set_mod_volume(50);
//   play_mod(mod,TRUE);



   rest(WAITTIME);

   
   draw_placeholder = get_config_int("MINER","DP",0);
   
   textout_ex(screen, fnt_sml,"Press any key to continue...", 5, textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   clear_keybuf();
   while(!keypressed());
   stretch_blit (title_back,screen,0,0,640,480, 0, 0, SCREEN_W, SCREEN_H);

   try_loop(&voice,SNDDATA_ZZZJ, -1,-1, 255,255,500);
   if (fade_speed)
   {
      if (bitmap_color_depth(screen) == 8)
          fade_from(grayscale_palette,game_palette,3);
      else
          effect(screen, 20, 0, 0);
   }
   else
      set_palette(game_palette);
      
   kill_loop(&voice);
   try_sample(SNDDATA_GONG2, -1,-1, 255,255,500);
   textout_centre_ex(screen,fnt_andrus,"MINER",310,230 - text_height(fnt_andrus)/2,makecol(0,0,0), -1);
   textout_centre_ex(screen,fnt_andrus,"MINER",319,240 - text_height(fnt_andrus)/2,makecol(0,0,255), -1);
   textout_centre_ex(screen,fnt_andrus,"MINER",321,240 - text_height(fnt_andrus)/2,makecol(0,0,255), -1);
   textout_centre_ex(screen,fnt_andrus,"MINER",320,241 - text_height(fnt_andrus)/2,makecol(0,0,255), -1);
   textout_centre_ex(screen,fnt_andrus,"MINER",320,239 - text_height(fnt_andrus)/2,makecol(0,0,255), -1);
   textout_centre_ex(screen,fnt_andrus,"MINER",320,240 - text_height(fnt_andrus)/2,makecol(0,255,0), -1);
   rest(30* WAITTIME);

   
   clear_keybuf();
   if (tight_mem)
   {
      if (snd_data)
      {
       destroy_sample((SAMPLE *)(snd_data[SNDDATA_GONG2].dat));
       snd_data[SNDDATA_GONG2].dat = NULL;
      }
   }
   install_mouse();
   set_window_title("Miner");

   if (system_driver->id == AL_ID('X','W','I','N'))
   {
      show_mouse(screen);
      
   }
   
}

void cleanup()
{
   int textpos = 0;

   stretch_blit (title_back,screen,0,0,640,480, 0, 0, SCREEN_W, SCREEN_H);

   if (fade_speed)
   {
      {
         if (bitmap_color_depth(screen) == 8)
         {
            get_palette(desktop_palette);
            fade_from(desktop_palette, grayscale_palette,4);
         }
      }

   }
   else
      set_palette(grayscale_palette);


   textout_ex(screen,fnt_sml,"Cleaning up engine...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   delete gfx_objects_table;
   delete proclist;
   delete drawlist;
   delete objcol;
   rest(WAITTIME);
   rest(WAITTIME);
   rest(WAITTIME);

   textout_ex(screen,fnt_sml,"Destroy graphics buffer...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   rest(WAITTIME);
   rest(WAITTIME);
   rest(WAITTIME);
   destroy_bitmap(screen_buffer);
   
   textout_ex(screen,fnt_sml,"Unload data...",5,textpos, makecol(0,0,0), -1);
   textpos+= text_height(fnt_sml);
   unload_data();
   
   rest(WAITTIME);
   rest(WAITTIME);
   rest(WAITTIME);
   
   if (fade_speed)
   {
      if (bitmap_color_depth(screen) == 8)
         fade_out(fade_speed);
      else
         effect(screen, 20, 0, 0);
   }

   cleanup_effect_tables();
   
   set_gfx_mode(GFX_TEXT,0,0,0,0);
   if (text_fade)
      set_palette(black_palette);


      
//   rest(100);
   printf("Byebye, hope you come back.\n");
   
   if (text_fade)
      fade_in(text_palette,1);

   allegro_exit();
}


/* for when the options dialog is used */
void reinit()
{
   int nr_voices;
   
   BPS = get_config_int("MINER","BPS",64);
   nr_voices = get_config_int("MINER","NR_OF_VOICES",8);
   install_int_ex(global_clock, BPS_TO_TIMER(BPS));

   remove_sound();

   reserve_voices(nr_voices,0);
   if (install_sound(DIGI_AUTODETECT,MIDI_NONE, NULL))
   {
//      textout_ex(screen, fnt_sml,"Could not install sound: silence", 15, textpos, makecol(0,0,0), -1);
      install_sound(DIGI_NONE,MIDI_NONE, NULL);
//      textpos+= text_height(fnt_sml);
   }
   else
   {
//      textpos+= text_height(fnt_sml);
   }


}
