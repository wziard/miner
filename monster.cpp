#include "monster.h"
//#include "stdio.h"
#include "stut.h"
#include "gfxdata.h"
#include "objidx.h"
#include "globals.h"

MONSTER::MONSTER(int initial_state)
          : COMPLEX_OBJECT()
{
   state = initial_state;
   create();
   
}

MONSTER::MONSTER()
          : COMPLEX_OBJECT()
{
   state = MONSTER_STATE_STAND;
   create();
}


MONSTER::~MONSTER()
{
}

void MONSTER::create()
{
   name = "MONSTER";
   idx = OBJ_MONSTER;

   if (!(gfx_objects_table->gfx_objects[idx]))
   {
      gfx_objects_table->init_object(idx, 5);
      gfx_objects_table->set_object_state(idx,MONSTER_STATE_STAND,(BITMAP *)(anims[GFXDATA_ANIMS_VOLG].dat));
      gfx_objects_table->set_object_state(idx,MONSTER_STATE_RIGHT,(BITMAP *)(anims[GFXDATA_ANIMS_VOLG].dat));
      gfx_objects_table->set_object_state(idx,MONSTER_STATE_LEFT,(BITMAP *)(anims[GFXDATA_ANIMS_VOLG].dat));
      gfx_objects_table->set_object_state(idx,MONSTER_STATE_UP,(BITMAP *)(anims[GFXDATA_ANIMS_VOLG].dat));
      gfx_objects_table->set_object_state(idx,MONSTER_STATE_DOWN,(BITMAP *)(anims[GFXDATA_ANIMS_VOLG].dat));
   }
   gfx_object = gfx_objects_table->gfx_objects[idx];
   
   tag = TAG_SMARTMONSTER;
   passflags |= PF_AXER;
   
   name = "MONSTER";
   my_layer = 1;
   my_frame = 0;
}

int MONSTER::proc()
{

   int px, py;
   
   frame++;
   frame %=32;

   try_loop_backwards(&voice,SNDDATA_MOEH, mapx, mapy, 255,10 ,1000);


   switch(state)
   {
      case MONSTER_STATE_UP:
       yoffs--;
       if (frame == 31)
       {
         move(0,-1);
         state = MONSTER_STATE_STAND;
       }

       break;
      case MONSTER_STATE_DOWN:
       yoffs++;
       if (frame == 31)
       {
         move(0,1);
         state = MONSTER_STATE_STAND;
       }

       break;
      case MONSTER_STATE_LEFT:
       xoffs--;
       if (frame == 31)
       {
         move(-1,0);
         state = MONSTER_STATE_STAND;
       }
       break;
      case MONSTER_STATE_RIGHT:
       xoffs++;
       if (frame == 31)
       {
         move(1,0);
         state = MONSTER_STATE_STAND;
       }
       break;
      default:
       frame = 0;
       xoffs=yoffs=0;
       px  = MONSTER_LOOK_DEPTH+1;
       py  = MONSTER_LOOK_DEPTH+1;
       for (int xx = -MONSTER_LOOK_DEPTH; xx < MONSTER_LOOK_DEPTH ; xx++)
       {
         for (int yy = -MONSTER_LOOK_DEPTH; yy < MONSTER_LOOK_DEPTH ; yy++)
         {
            if (playfield->contains_type(mapx+xx,mapy+yy,TAG_PLAYER))
            {

               if (px * px + py * py > xx*xx + yy * yy)
               {
                  px = xx;
                  py = yy;
               }
            }
         }
       }
       if (px > MONSTER_LOOK_DEPTH)
       {
          kill_loop(&voice);
          return F_DONE;
       }

       if (px <0)
       {
         if (py <0)
         {
            if (px < py)
            {
               if (playfield->allowed_move(mapx -1, mapy, PF_MONSTER))
               {
                  state = MONSTER_STATE_LEFT;
                  playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx, mapy-1, PF_MONSTER))
               {
                  state = MONSTER_STATE_UP;
                  playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
            else
            {
               if (playfield->allowed_move(mapx , mapy-1, PF_MONSTER))
               {
                  state = MONSTER_STATE_UP;
                  playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx-1, mapy, PF_MONSTER))
               {
                  playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
                  state = MONSTER_STATE_LEFT;
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
         }
         else
         {
            if (px < -py)
            {
               if (playfield->allowed_move(mapx -1, mapy, PF_MONSTER))
               {
                  state = MONSTER_STATE_LEFT;
                  playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx, mapy+1, PF_MONSTER) && py)
               {
                  state = MONSTER_STATE_DOWN;
                  playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
            else
            {
               if (playfield->allowed_move(mapx , mapy+1, PF_MONSTER) && py)
               {
                  state = MONSTER_STATE_DOWN;
                  playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx-1, mapy, PF_MONSTER))
               {
                  state = MONSTER_STATE_LEFT;
                  playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }


         }
       }
       else
       {
         if (py <0)
         {
            if (px > py)
            {
               if (playfield->allowed_move(mapx +1, mapy, PF_MONSTER) &&px)
               {
                  state = MONSTER_STATE_RIGHT;
                  playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx, mapy-1, PF_MONSTER))
               {
                  state = MONSTER_STATE_UP;
                  playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
            else
            {
               if (playfield->allowed_move(mapx , mapy-1, PF_MONSTER))
               {
                  state = MONSTER_STATE_UP;
                  playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx+1, mapy, PF_MONSTER) && px)
               {
                  state = MONSTER_STATE_RIGHT;
                  playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
         }
         else
         {
            if (px > py)
            {
               if (playfield->allowed_move(mapx +1, mapy, PF_MONSTER))
               {
                  state = MONSTER_STATE_RIGHT;
                  playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx, mapy+1, PF_MONSTER) && py)
               {
                  state = MONSTER_STATE_DOWN;
                  playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
            else
            {
               if (playfield->allowed_move(mapx , mapy+1, PF_MONSTER) && py)
               {
                  state = MONSTER_STATE_DOWN;
                  playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else if (playfield->allowed_move(mapx+1, mapy, PF_MONSTER) && px)
               {
                  playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
                  state = MONSTER_STATE_RIGHT;
               }
               else
               {
                  state = MONSTER_STATE_STAND;
                  kill_loop(&voice);
                  return F_DONE;
               }
               
            }
         }
       }
       
       return F_CONTINUE;
       break;
   }

   return F_CONTINUE;
}


void MONSTER::die()
{

   try_sample(SNDDATA_AARG,mapx, mapy, 200);
   OBJECT::die();
}
