#include "explode.h"
#include "boem.h"
#include "globals.h"
#include "gfxdata.h"

void explode(int mapx, int mapy, int r)
{
   int x,y;
 
   try_sample(SNDDATA_BOOM, mapx,mapy,200);
 
   for (int i = -r;i<=r;i++)
   {
      for (int j = -r;j<=r;j++)
      {
         if (i*i+j*j<=r*r && ( i || j))
         {
            x=mapx+i;
            y=mapy+j;
            if (x>0 && y>0 && x<playfield->w-1 && y<playfield->h-1)
            {
               playfield->set(x,y,objcol->new_obj(OBJ_BOEM));
            }
         }
      }
   }
}
