#include "globals.h"
#include "gfxdata.h"
#include "brick.h"
#include "objidx.h"

BRICK::BRICK()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BRICK].dat);
   tag = TAG_MONSTER;
   passflags |= PF_PLAYER;
   score.points = 1;
   name ="BRICK";
   idx = OBJ_BRICK;
}

int BRICK::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

BRICK2::BRICK2()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BRICK2].dat);
   tag = TAG_MONSTER;
   passflags |= PF_PLAYER;
   score.points = 10;
   name ="BRICK2";
   idx = OBJ_BRICK2;
}

int BRICK2::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

BRICK3::BRICK3()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BRICK3].dat);
   tag = TAG_MONSTER;
   passflags |= PF_PLAYER;
   score.points = 10;
   name ="BRICK3";
   idx = OBJ_BRICK3;
}

int BRICK3::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

