#ifndef _NE_RE_H_
#define _NE_RE_H_

#include "objects.h"


#define NE_RE_STATE_STANDING 0
#define NE_RE_STATE_FALLING 1
#define NE_RE_STATE_SLIDING 2


class NE_RE
    : public STATIC_OBJECT
{
  public:
   NE_RE();
   int proc();
  private:
};

#endif
