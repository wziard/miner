#ifndef _LINKS_H_
#define _LINKS_H_

#include "objects.h"


#define LINKS_STATE_STANDING 0
#define LINKS_STATE_FALLING 1

class LINKS
    : public STATIC_OBJECT
{
  public:
   LINKS();
   int proc();
  private:
   int speed;
};

#endif
