#ifndef _BOEM_H_
#define _BOEM_H_
#include "objects.h"

class BOEM
    : public ANIMATED_OBJECT
{
  public:
   BOEM();
   int proc();
   int can_coexist(int tag);
};


#endif
