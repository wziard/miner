#include "globals.h"
#include "gfxdata.h"
#include "rechts.h"
#include "objidx.h"

RECHTS::RECHTS()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_RECHTS].dat);   
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = RECHTS_STATE_STANDING;
   speed = 1;
   name = "RECHTS";
   idx = OBJ_RECHTS;
}

int RECHTS::proc()
{
   OBJECT *collide_monster;
   frame++;
   frame %= (32 / speed);
   switch(state)
   {

      case RECHTS_STATE_FALLING:
        try_loop(&voice,SNDDATA_GGGG, mapx, mapy,100);
        if (frame == (32 / speed) - 1)
        {
            move(1,0);
            if (!playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
            {
               kill_loop(&voice);
               state = RECHTS_STATE_STANDING;
               collide_monster = playfield->contains_type(mapx+1,mapy, TAG_SMARTMONSTER);
               if (collide_monster && (speed >= 4))
                     collide_monster->die();
               
            }
            else
            {
               speed = MIN(speed<<1, 8);
               playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            xoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        if (playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
        {
            state = RECHTS_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
