#ifndef _BACKGROUND_H_
#define _BACKGROUND_H_
#include "objects.h"

class BASE_BACKGROUND
      : public OBJECT
{
   public:
      BASE_BACKGROUND();
      void draw(BITMAP *onto, int x, int y, int layer);
      
      RLE_SPRITE *sprite;
};



class BACKGROUND
    : public BASE_BACKGROUND
{
   public:
      BACKGROUND();
      int proc();
};


class BACKGROUND2
    : public BASE_BACKGROUND
{
   public:
      BACKGROUND2();
      int proc();
};

class BACKGROUND3
    : public BASE_BACKGROUND
{
   public:
      BACKGROUND3();
      int proc();
};

#endif
