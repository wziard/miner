#ifndef _NET_H_
#define _NET_H_

#include "objects.h"

#define NET_STATE_STAND 0
#define NET_STATE_RIGHT 1
#define NET_STATE_LEFT  2
#define NET_STATE_UP    3
#define NET_STATE_DOWN  4


class NET
    : public COMPLEX_OBJECT
{
   public:
          NET(int generations = 10);
          NET(int initial_state, int generation);
          ~NET();
          int proc();
   private:
      void create();
      int generation;
      void set_state(int state,int generation);
};


#endif
