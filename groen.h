#ifndef _GROEN_H_
#define _GROEN_H_

#include "objects.h"

#define GROEN_STATE_STAND 0
#define GROEN_STATE_RIGHT 1
#define GROEN_STATE_LEFT  2
#define GROEN_STATE_UP    3
#define GROEN_STATE_DOWN  4


class GROEN
    : public COMPLEX_OBJECT
{
   public:
          GROEN();
          GROEN(int initial_state);
          ~GROEN();
          int proc();
   private:
      void create();
      int my_frame;
};


#endif
