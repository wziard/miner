#include "globals.h"
#include "data.h"
#include "bomb.h"
#include "explode.h"
#include "objidx.h"


BOMB::BOMB()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BOMB].dat);
   tag = TAG_STRUCTURE;
   passflags |= PF_PLAYER;
   passflags |= PF_SMARTMONSTER;
   name ="BOMB";
   idx = OBJ_BOMB;
}

int BOMB::proc()
{
   if (playfield->contains_type(mapx,mapy,TAG_PLAYER))
   {
      explode(mapx,mapy);
      return F_DELETE;
   }
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

int BOMB::can_coexist(int tag)
{
    if (tag == TAG_PLAYER || tag == TAG_SMARTMONSTER || tag == TAG_BACKGROUND)
       return TRUE;

    return FALSE;
}
