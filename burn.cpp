#include "burn.h"
//#include "stdio.h"
#include "stut.h"
#include "gfxdata.h"
#include "objidx.h"
#include "globals.h"

BURN::BURN(int initial_state)
          : COMPLEX_OBJECT()
{
   state = initial_state;
   create();
   
}

BURN::BURN()
          : COMPLEX_OBJECT()
{
   state = BURN_STATE_STAND;
   create();
}


BURN::~BURN()
{
}

void BURN::create()
{
   name = "BURN";
   idx = OBJ_BURN;

   if (!(gfx_objects_table->gfx_objects[idx]))
   {

      gfx_objects_table->init_object(idx, 5);
      gfx_objects_table->set_object_state(idx,BURN_STATE_STAND,(BITMAP *)(anims[GFXDATA_ANIMS_VUUR].dat));
      gfx_objects_table->set_object_state(idx,BURN_STATE_RIGHT,(BITMAP *)(anims[GFXDATA_ANIMS_VUURR].dat));
      gfx_objects_table->set_object_state(idx,BURN_STATE_LEFT,(BITMAP *)(anims[GFXDATA_ANIMS_VUURL].dat));
      gfx_objects_table->set_object_state(idx,BURN_STATE_UP,(BITMAP *)(anims[GFXDATA_ANIMS_VUURO].dat));
      gfx_objects_table->set_object_state(idx,BURN_STATE_DOWN,(BITMAP *)(anims[GFXDATA_ANIMS_VUURN].dat));
   }
   gfx_object = gfx_objects_table->gfx_objects[idx];

   tag = TAG_STRUCTURE;
   name = "BURN";
   my_layer = 1;
   my_frame = 0;
}

int BURN::proc()
{
   OBJECT *here;
   my_frame++;
   my_frame %= 3;

   if ((here = playfield->contains_type(mapx,mapy,TAG_PLAYER)))
        here->die();
   
   if (!my_frame)
      frame++;
      
   frame %=32;

   switch(state)
   {
      case BURN_STATE_UP:
      case BURN_STATE_DOWN:
      case BURN_STATE_LEFT:
      case BURN_STATE_RIGHT:
       try_loop(&voice,SNDDATA_KNETTER, mapx, mapy, 255,10 ,100);
       
       if (frame == 31)
       {
          kill_loop(&voice);
          state = BURN_STATE_STAND;
       }
       break;
      default:
         if (frame == 5)
         {
            if (playfield->contains_object(mapx,mapy - 1, OBJ_GROEN) && !playfield->contains_object(mapx,mapy - 1, OBJ_BURN))
               playfield->set(mapx, mapy - 1, objcol->new_obj(OBJ_BURN, BURN_STATE_UP));

            if (playfield->contains_object(mapx,mapy + 1, OBJ_GROEN) && !playfield->contains_object(mapx,mapy + 1, OBJ_BURN))
               playfield->set(mapx, mapy + 1, objcol->new_obj(OBJ_BURN, BURN_STATE_DOWN));
      
            if (playfield->contains_object(mapx - 1,mapy , OBJ_GROEN) && !playfield->contains_object(mapx-1,mapy, OBJ_BURN))
               playfield->set(mapx - 1, mapy,objcol->new_obj(OBJ_BURN, BURN_STATE_LEFT));

            if (playfield->contains_object(mapx + 1,mapy , OBJ_GROEN) && !playfield->contains_object(mapx+1,mapy, OBJ_BURN))
               playfield->set(mapx + 1, mapy, objcol->new_obj(OBJ_BURN, BURN_STATE_RIGHT));
         }
         if (frame == 30)
          return F_DELETE;

         return F_CONTINUE;
         
         break;
   }

   return F_CONTINUE;
}

int BURN::can_coexist(int other)
{
   if (other == TAG_STRUCTURE)
      return FALSE;
   return TRUE;
}



void BURN::set_state(int _state, int extra)
{
   extra = 0;
   state = _state;
   frame = (rand() % 5);

}
