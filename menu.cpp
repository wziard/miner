#include "globals.h"
#include "mainloop.h"
#include "gfxdata.h"
#include "optmenu.h"
#include "effect.h"

#define MM_STATE_PLAY    0
#define MM_STATE_OPTIONS 1
#define MM_STATE_EXIT    2



int same_palette(RGB *p1, RGB *p2)
{

   for (int i=0;i<256;i++)
   {
      if (p1[i].r != p2[i].r)
         return FALSE;

      if (p1[i].g != p2[i].g)
         return FALSE;
   
      if (p1[i].b != p2[i].b)
         return FALSE;
   }

   return TRUE;
}

static void menu_textout(char *string, int y, int color)
{
   textout_centre_ex(screen_buffer,fnt_andrus,string,321, y,makecol(0,0,0),-1);
   textout_centre_ex(screen_buffer,fnt_andrus,string,320, y+1,makecol(0,0,0),-1);
   textout_centre_ex(screen_buffer,fnt_andrus,string,321, y+1,makecol(0,0,0),-1);
   textout_centre_ex(screen_buffer,fnt_andrus,string,320, y,color,-1);


}


static int acolor,icolor;

void mainmenu()
{

   int state = MM_STATE_PLAY;
   int key=0;
   acolor = makecol(0,255,0);
   icolor = makecol(0,100,0);
   if (tight_mem)
      clear_to_color(screen_buffer, makecol(255,0,0));
   else
      stretch_blit (title_back,screen_buffer,0,0,640,480, 0, 0, screen_buffer->w, screen_buffer->h);
   
   while (state != MM_STATE_EXIT)
   {
      key=0;
      clear_keybuf();
      
      if (tight_mem)
         clear_to_color(screen_buffer, makecol(255,0,0));
      else
         stretch_blit (title_back,screen,0,0,640,480, 0, 0, SCREEN_W, SCREEN_H);

      stretch_blit(screen_buffer,screen,0,0,screen_buffer->w, screen_buffer->h, 0,0,SCREEN_W,SCREEN_H);
      if (fade_speed)
      {
         get_palette(desktop_palette);
         if (!same_palette(desktop_palette, game_palette))
            fade_from(desktop_palette,game_palette,fade_speed);
      }
      else
         set_palette(game_palette);
         
      while(!((key) == KEY_ENTER))
      {
         if (tight_mem)
            clear_to_color(screen_buffer, makecol(255,0,0));
         else
            stretch_blit (title_back,screen_buffer,0,0,640,480, 0, 0, SCREEN_W, SCREEN_H);
             
         switch(state)
         {
          case MM_STATE_PLAY:
           menu_textout((char *)"PLAY",100,acolor);
           menu_textout((char *)"options",220,icolor);
           menu_textout((char *)"exit",340,icolor);
           break;
          case MM_STATE_OPTIONS:
           menu_textout((char *)"play",100,icolor);
           menu_textout((char *)"OPTIONS",220,acolor);
           menu_textout((char *)"exit",340,icolor);
           break;
          case MM_STATE_EXIT:
           menu_textout((char *)"play",100,icolor);
           menu_textout((char *)"options",220,icolor);
           menu_textout((char *)"EXIT",340,acolor);
           break;
         }
         stretch_blit(screen_buffer,screen,0,0,screen_buffer->w, screen_buffer->h, 0,0,SCREEN_W,SCREEN_H);
         
         key = readkey() >> 8;

         if (key== KEY_UP)
         {
            try_sample(SNDDATA_ZZZJ, -1,-1, 255,255,1000);
            state+=2;
         }
         if (key == KEY_DOWN)
         {
            try_sample(SNDDATA_ZZZJ, -1,-1, 255,255,1000);
            state++;
         }
         state %=3;
      }
      try_sample(SNDDATA_GONG, -1,-1, 255,255,500);
      
      if (fade_speed)
      {
         if (bitmap_color_depth(screen) == 8)
             fade_out(fade_speed);
         else
             effect(screen, 20, 0, 0);
      }
      else
         set_palette(black_palette);

      if (state == MM_STATE_PLAY)
         oneplayer_game();

      if (state == MM_STATE_OPTIONS)
         options_menu();
   }


}
