#include "globals.h"
#include "balloon.h"
#include "objidx.h"

BALLOON::BALLOON()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BALLOON].dat);   
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = BALLOON_STATE_STANDING;
   speed = 1;
   name = "BALLOON";
   idx = OBJ_BALLOON;
}

int BALLOON::proc()
{
   frame++;
   frame %= (32 / speed);

   
   switch(state)
   {

      case BALLOON_STATE_FALLING:
        if (frame == (32 / speed) - 1)
        {
            move(0,-1);

            if (!playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
               state = BALLOON_STATE_STANDING;
            else
            {
               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy-1, objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs-=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        if (playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
        {
            state = BALLOON_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
            try_sample(SNDDATA_WOESH,mapx, mapy, 200);
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
