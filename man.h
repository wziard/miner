#ifndef _MAN_H_
#define _MAN_H_

#include "objects.h"
#include "globals.h"

#define MAN_STATE_STAND 0
#define MAN_STATE_WALKR 1
#define MAN_STATE_WALKL 2
#define MAN_STATE_WALKU 3
#define MAN_STATE_WALKD 4
#define MAN_STATE_HAK   5
#define MAN_STATE_HAKR  6
#define MAN_STATE_HAKL  7
#define MAN_STATE_HAKU  8
#define MAN_STATE_HAKD  9
#define MAN_STATE_HOERA 10
#define MAN_STATE_OH    11
#define MAN_STATE_BOIL  12
#define MAN_STATE_MELT  13
#define MAN_STATE_TIL   14


class MAN
    : public COMPLEX_OBJECT
{
   public:
          MAN(int nr_of_lives);
          ~MAN();
          int proc();
          int gameover();
          void die();
          void set_state(int lives, int unused = 0);
   private:
           // man already contains a score, but that's the score for eating him!
          Score my_score;
          void move(int dx, int dy);
          int stars_to_get;
          
};


#endif
