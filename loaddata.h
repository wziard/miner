#ifndef _LOADDATA_H_
#define _LOADDATA_H_

extern void load_gfx_data();
extern void load_snd_data();
extern void unload_data();
#endif
