#include "globals.h"
#include "objects.h"
#include "objidx.h"

STATIC_OBJECT::STATIC_OBJECT()
{

}

void STATIC_OBJECT::draw(BITMAP *onto, int x, int y, int layer)
{
   if (layer !=1)
     return;//only draw if layer =1
   draw_rle_sprite(onto, sprite, x  + xoffs, y + yoffs);
}

void ANIMATED_OBJECT::draw(BITMAP *onto,int x, int y, int layer)
{
   if (layer !=1)
     return;//only draw if layer =1

   int real_frame = (nr_of_frames - 1) * frame /31;

   blit(sprites,tmp_sprite,real_frame * 32,0,0,0,32,32);
   draw_sprite(onto, tmp_sprite, x + xoffs, y + yoffs);

//   destroy_bitmap(currsprite);
}

int ANIMATED_OBJECT::instances = 0;
BITMAP *ANIMATED_OBJECT::tmp_sprite = NULL;

ANIMATED_OBJECT::ANIMATED_OBJECT(BITMAP *_sprites)
{
    if (!instances)
        tmp_sprite = create_bitmap(32,32);

    instances++;
    sprites = _sprites;
    nr_of_frames = sprites->w / TILESIZE_X;
    
}

ANIMATED_OBJECT::~ANIMATED_OBJECT()
{
    instances--;

    if (!instances)
        destroy_bitmap(tmp_sprite);
}




COMPLEX_OBJECT::COMPLEX_OBJECT()
{
    gfx_object = NULL;
    frame = 0;
    state = 0;

}


COMPLEX_OBJECT::~COMPLEX_OBJECT()
{

}

void COMPLEX_OBJECT::draw(BITMAP *onto, int x, int y, int layer)
{
   if (layer != my_layer)
     return;//only draw if layer =2

   if (!gfx_object)
      printerror("object not initialised when drawn");

   gfx_object->draw(onto,x + xoffs, y+yoffs,state, frame);
}
PLACEHOLDER::PLACEHOLDER()
{
   passflags = PF_NONE_SHALL_PASS;
   tag = TAG_PLACEHOLDER;// it will be deleted by the player when he lands on it
   frame = 0;
   name = "PLACEHOLDER";
   idx = OBJ_PLACEHOLDER;
}

int PLACEHOLDER::proc()
{
   frame++;
   
//     fprintf(stderr,"placeholder %d frame = %d\n", id, frame);
   
   if (frame > 128) // disappear after 31 frames, if the placer is not yet her, he can forget it
   {
      return F_DELETE;
   }
   return F_CONTINUE;
}

void PLACEHOLDER::draw(BITMAP *onto,int x, int y, int l)
{
//   onto = NULL;
//   x = y = l = 0;
   if (draw_placeholder && !l)
      circlefill(onto,x+xoffs+16, y+yoffs+16,16,1);

   return;
}

int PLACEHOLDER::can_coexist(int tag)
{
 tag = 0;
 return TRUE;
}



GFX_OBJECT::GFX_OBJECT(int _nr_of_states)
{
  nr_of_states = _nr_of_states;
  states = (ANIM *)malloc(sizeof(ANIM) * nr_of_states);

  if (!states)
     out_of_mem("allocating animation tables");

  for (int i=0;i<nr_of_states;i++)
  {
      states[i].last_frame = 0;
      states[i].frames = NULL;
  }

  
}

GFX_OBJECT::~GFX_OBJECT()
{
   for (int i=0;i<nr_of_states;i++)
   {
      if (states[i].frames)
      {
        for (int j=0;j<=states[i].last_frame;j++)
         destroy_bitmap(states[i].frames[j]);

        free (states[i].frames);
      }
   }
   
   free(states);
   

}

void GFX_OBJECT::set_state_anim(int _state, BITMAP *strip)
{
   if (states[_state].frames)
      printerror("object state initialised twice (state %d)",_state);
      
   states[_state].last_frame = (strip->w / TILESIZE_X)- 1;
   states[_state].frames =  (BITMAP **)malloc(sizeof(BITMAP *)  * (states[_state].last_frame+1));
   if (!(states[_state].frames))
      out_of_mem("allocating animation tables");

   for (int i=0;i<=states[_state].last_frame;i++)
   {
       states[_state].frames[i] = create_sub_bitmap(strip, i * TILESIZE_X, 0, TILESIZE_X, TILESIZE_Y);
       if (!(states[_state].frames[i]))
          out_of_mem("creating animation frames");
   }
}


void GFX_OBJECT::draw(BITMAP *onto, int x, int y, int state, int frame)
{
   static int trueframe;
   trueframe = frame * (states[state].last_frame) / 31;
   masked_blit(states[state].frames[trueframe], onto, 0,0,x,y,TILESIZE_X,TILESIZE_Y);

}


GFX_OBJECTS_TABLE::GFX_OBJECTS_TABLE(int nr_of_objects)
{
   //_MM_MARK();
   nr = nr_of_objects;
   //_MM_MARK();
   gfx_objects = (GFX_OBJECT **)malloc(sizeof(GFX_OBJECT *) * nr);
   //_MM_MARK();

   if (!gfx_objects)
      out_of_mem("allocating gfx tables");
   //_MM_MARK();

   for (int i=0;i<nr;i++)
   {
     gfx_objects[i] = NULL;
   }
   //_MM_MARK();
}


GFX_OBJECTS_TABLE::~GFX_OBJECTS_TABLE()
{
   for (int i=0;i<nr;i++)
   {
     if (gfx_objects[i])
        delete gfx_objects[i];
   }
   free(gfx_objects);
}

void GFX_OBJECTS_TABLE::init_object(int obj,int nr_of_states)
{

   if (gfx_objects[obj])
      printerror("object %d initilised twice",obj);

   gfx_objects[obj] = new GFX_OBJECT(nr_of_states);
      
}

void GFX_OBJECTS_TABLE::set_object_state(int obj, int state, BITMAP *strip)
{
   if (!(gfx_objects[obj]))
      printerror("object %d not initialised",obj);

   gfx_objects[obj]->set_state_anim(state,strip);
}

void GFX_OBJECTS_TABLE::draw(BITMAP *onto,int x, int y, int object, int state, int frame)
{
   if (!(gfx_objects[object]))
      printerror("object %d not initialised (tried to draw it)",object);


   gfx_objects[object]->draw(onto,x,y,state,frame);

}



