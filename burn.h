#ifndef _BURN_H_
#define _BURN_H_

#include "objects.h"

#define BURN_STATE_STAND 0
#define BURN_STATE_RIGHT 1
#define BURN_STATE_LEFT  2
#define BURN_STATE_UP    3
#define BURN_STATE_DOWN  4


class BURN
    : public COMPLEX_OBJECT
{
   public:
          BURN();
          BURN(int initial_state);
          ~BURN();
          int proc();
          int can_coexist(int other);
          void set_state(int state, int extra);
   private:
      void create();
      int my_frame;
};


#endif





