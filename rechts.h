#ifndef _RECHTS_H_
#define _RECHTS_H_

#include "objects.h"


#define RECHTS_STATE_STANDING 0
#define RECHTS_STATE_FALLING 1

class RECHTS
    : public STATIC_OBJECT
{
  public:
   RECHTS();
   int proc();
  private:
   int speed;
};

#endif
