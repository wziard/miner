#include <stdarg.h>
#include <stdio.h>
#include "allegr.h"
#include "helpers.h"



void out_of_mem(char const *msg)
{
   allegro_exit();

   if (msg)
      fprintf(stderr,"Out of memory: %s",msg);
   else
      fprintf(stderr,"Out of memory");
   exit(1);
}

static char buffer[1000];

void printerror(char const *format, ...)
{
   va_list arg;
   va_start(arg, format);
   vsprintf(buffer, format, arg);
   va_end(arg);
   
   allegro_exit();
   fprintf(stderr, "ERROR:%s\n",buffer);
   exit(1);
}


void warn(char const *format, ...)
{
   va_list arg;
   va_start(arg, format);
   vsprintf(buffer, format, arg);
   va_end(arg);
   if (screen)
      alert("Warning:",buffer,NULL,"OK", NULL, 13,0);
   else
   {
      fprintf(stderr,"Warning:\n%s\n", buffer);
      while(!keypressed());
   }

}

