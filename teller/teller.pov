
#include "colors.inc"


#declare Wiel = cylinder
{
   0,y,2

   texture
   {
      pigment
      {
         image_map
         {
          tga
          "number.tga"
          map_type 2
         }
      }
   }
   texture
   {
    pigment{color rgbt <1,1,.5,.6>}
//    finish{phong 1}
   }

}


#declare Frame = difference
{
   box { <-6,-11,-2><6,11,-1.9>}
   box { <-.5,-1,-3><.5,1,0>}
   pigment{color rgb <.1,.1,.1>}
   normal{bumps scale .01}
   finish{phong 1}

}

object{Wiel rotate z * 90 translate x * .5 rotate x * (clock * 360 - 72)}
object{Frame}



light_source
{
   <-10,100,-100>
   White
}

camera
{
 orthographic
 up  y * 2.2
 right  x * 1.2
 location z*-6
 look_at 0


}
