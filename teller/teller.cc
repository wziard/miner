#include "allegro.h"




void main()
{
   allegro_init();
   set_gfx_mode(GFX_AUTODETECT,320,200,0,0);
   BITMAP *out = create_bitmap(10, 100);
   set_palette(desktop_palette);

   clear_to_color(out,makecol(0,0,0));
   text_mode(makecol(0,0,0));
   textout(out,font,"0",1,1,makecol(255,255,255));
   textout(out,font,"1",1,11,makecol(255,255,255));
   textout(out,font,"2",1,21,makecol(255,255,255));
   textout(out,font,"3",1,31,makecol(255,255,255));
   textout(out,font,"4",1,41,makecol(255,255,255));
   textout(out,font,"5",1,51,makecol(255,255,255));
   textout(out,font,"6",1,61,makecol(255,255,255));
   textout(out,font,"7",1,71,makecol(255,255,255));
   textout(out,font,"8",1,81,makecol(255,255,255));
   textout(out,font,"9",1,91,makecol(255,255,255));

   save_bitmap("number.tga",out,desktop_palette);


}
