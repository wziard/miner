

#include <allegro.h>




int main(int argc, char **argv)
{
 allegro_init();
 set_gfx_mode(GFX_VGA,320,200,0,0);
 BITMAP *in;
 BITMAP *out = create_bitmap(16*320,32);
 for (int i=0;i<320;i++)
 {
  in = load_bitmap(argv[i+1],desktop_palette);
  blit(in,out,0,0,16*i,0,16,32);
  destroy_bitmap(in);
 }

 save_bitmap("geplakt.bmp",out,desktop_palette);
}
