#include "globals.h"
#include "gfxdata.h"
#include "ne_li.h"
#include "objidx.h"

NE_LI::NE_LI()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_NELI].dat);
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = NE_LI_STATE_STANDING;
   name = "NE_LI";
   idx = OBJ_NE_LI;
}

int NE_LI::proc()
{
   frame++;
   frame %= 32;
   
   
   switch(state)
   {

      case NE_LI_STATE_FALLING:
        try_loop(&voice,SNDDATA_GGGG, mapx, mapy,100,128,2000);
        if (frame == 31)
        {
            move(0,+1);
            yoffs=0;
            if (!playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
            {
               try_sample(SNDDATA_TIK,mapx,mapy,200,128,100);
               if (playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
               {
                  kill_loop(&voice);
                  state = NE_LI_STATE_SLIDING;
                  playfield->set(mapx - 1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               }
               else
                  state = NE_LI_STATE_STANDING;

            }
            else
                playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));

        }
        else
        {
            yoffs+=1;
        }
        return F_CONTINUE;
        break;
      case NE_LI_STATE_SLIDING:
        try_loop(&voice,SNDDATA_GGGG, mapx, mapy,100);
        if (frame == 31)
        {
            move(-1,0);
            xoffs=0;

            if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
            {
               kill_loop(&voice);
               state = NE_LI_STATE_FALLING;
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
            else if (!playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
            {
                 state =  NE_LI_STATE_STANDING;
                 try_sample(SNDDATA_TIK,mapx,mapy,200,128,100);
            }
         else
                 playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            
        }
        else
        {
            xoffs-=1;
        }
        return F_CONTINUE;
        break;
      default:
        kill_loop(&voice);
        if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
        {
            state = NE_LI_STATE_FALLING;
            frame = 0;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->allowed_move(mapx - 1,mapy,PF_MONSTER))
        {
            state = NE_LI_STATE_SLIDING;
            frame = 0;
            playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        
        break;
   }
   return F_DONE;
}
