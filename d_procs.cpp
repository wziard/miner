#include <stdio.h>

#include "globals.h"
#include "gfxdata.h"
#include "teller.h"


static void dialog_textout(char *string, int x, int y, int color)
{
   textout_ex(screen,fnt_andrus_4,string,x+1, y,makecol(0,0,0),-1);
   textout_ex(screen,fnt_andrus_4,string,x, y+1,makecol(0,0,0),-1);
   textout_ex(screen,fnt_andrus_4,string,x+1, y+1,makecol(0,0,0),-1);
   textout_ex(screen,fnt_andrus_4,string,x, y,color,-1);
}




int d_seltext_proc(int msg, DIALOG *d, int c)
{
   int acolor = makecol(0,255,0);
   int icolor = makecol(0,100,0);
   int ret = D_O_K;

   if (msg != MSG_DRAW)
      ret = d_radio_proc(msg, d, c);

//   if (msg == MSG_GOTFOCUS)
//   {
//      ret = d_radio_proc(MSG_KEY, d, c);
//   }
   switch (msg)
   {
      case MSG_START:
           d->w = text_length(fnt_andrus_4, ((char *)d->dp));
           d->h = text_height(fnt_andrus_4);
           break;
      case MSG_DRAW:
           if (d->flags & D_SELECTED)
           {
               char *t = strdup((char *)d->dp);
               dialog_textout(ustrupr(t),d->x, d->y , acolor);
               free(t);
           }
           else
           {
               char *t = strdup((char *)d->dp);
               dialog_textout(ustrlwr(t),d->x, d->y , icolor);
               free(t);
           }
           break;
   
   }

   return ret;
}



int d_number_proc(int msg, DIALOG *d, int c)
{
   TELLER *t = (TELLER *)d->dp;

   switch (msg)
   {
      case MSG_START:
           d->dp = t = new TELLER(d->d1);
           t->set(d->d2);
           break;
      case MSG_END:
           delete t;
           d->dp = NULL;
           break;
      case MSG_IDLE:
           t->turn(d->d2);
           t->proc();
           SEND_MESSAGE(d, MSG_DRAW,  c);
           break;
           // fall through
      case MSG_DRAW:
           t->draw(screen, d->x, d->y);
           break;
   }

   return D_O_K;
}


