/* Allegro datafile object indexes, produced by grabber v3.9.32 (WIP), Unix */
/* Datafile: /home/martijn/projects/col/data.dat */
/* Date: Wed Apr 19 14:13:11 2000 */
/* Do not hand edit! */

#define GFXDATA                          0        /* FILE */

#define GFXDATA_ANIMS                    0        /* FILE */

#define GFXDATA_ANIMS_BOEM               0        /* BMP  */
#define GFXDATA_ANIMS_BOIL               1        /* BMP  */
#define GFXDATA_ANIMS_BREAK              2        /* BMP  */
#define GFXDATA_ANIMS_BURN               3        /* BMP  */
#define GFXDATA_ANIMS_BUSH               4        /* BMP  */
#define GFXDATA_ANIMS_BUSHL              5        /* BMP  */
#define GFXDATA_ANIMS_BUSHN              6        /* BMP  */
#define GFXDATA_ANIMS_BUSHO              7        /* BMP  */
#define GFXDATA_ANIMS_BUSHR              8        /* BMP  */
#define GFXDATA_ANIMS_FLAME              9        /* BMP  */
#define GFXDATA_ANIMS_HAKL               10       /* BMP  */
#define GFXDATA_ANIMS_HAKN               11       /* BMP  */
#define GFXDATA_ANIMS_HAKO               12       /* BMP  */
#define GFXDATA_ANIMS_HAKR               13       /* BMP  */
#define GFXDATA_ANIMS_HOERA              14       /* BMP  */
#define GFXDATA_ANIMS_MANHAK             15       /* BMP  */
#define GFXDATA_ANIMS_MANL               16       /* BMP  */
#define GFXDATA_ANIMS_MANN               17       /* BMP  */
#define GFXDATA_ANIMS_MANO               18       /* BMP  */
#define GFXDATA_ANIMS_MANOH              19       /* BMP  */
#define GFXDATA_ANIMS_MANR               20       /* BMP  */
#define GFXDATA_ANIMS_MANS               21       /* BMP  */
#define GFXDATA_ANIMS_MANT               22       /* BMP  */
#define GFXDATA_ANIMS_MANZINK            23       /* BMP  */
#define GFXDATA_ANIMS_NET                24       /* BMP  */
#define GFXDATA_ANIMS_NETD               25       /* BMP  */
#define GFXDATA_ANIMS_NETL               26       /* BMP  */
#define GFXDATA_ANIMS_NETR               27       /* BMP  */
#define GFXDATA_ANIMS_NETU               28       /* BMP  */
#define GFXDATA_ANIMS_VOLG               29       /* BMP  */
#define GFXDATA_ANIMS_VUUR               30       /* BMP  */
#define GFXDATA_ANIMS_VUURL              31       /* BMP  */
#define GFXDATA_ANIMS_VUURN              32       /* BMP  */
#define GFXDATA_ANIMS_VUURO              33       /* BMP  */
#define GFXDATA_ANIMS_VUURR              34       /* BMP  */
#define GFXDATA_ANIMS_COUNT              35

#define GFXDATA_FONTS                    1        /* FILE */

#define GFXDATA_FONTS_ANDRUS             0        /* FONT */
#define GFXDATA_FONTS_ANDRUS_4           1        /* FONT */
#define GFXDATA_FONTS_CAROL              2        /* FONT */
#define GFXDATA_FONTS_COUNT              3

#define GFXDATA_OBJECTS                  2        /* FILE */

#define GFXDATA_OBJECTS_BACKGROUND       0        /* RLE  */
#define GFXDATA_OBJECTS_BACKGROUND2      1        /* RLE  */
#define GFXDATA_OBJECTS_BACKGROUND3      2        /* RLE  */
#define GFXDATA_OBJECTS_BALLOON          3        /* RLE  */
#define GFXDATA_OBJECTS_BLOKAL           4        /* RLE  */
#define GFXDATA_OBJECTS_BOMAL            5        /* RLE  */
#define GFXDATA_OBJECTS_BOMB             6        /* RLE  */
#define GFXDATA_OBJECTS_BOX              7        /* RLE  */
#define GFXDATA_OBJECTS_BRICK            8        /* RLE  */
#define GFXDATA_OBJECTS_BRICK2           9        /* RLE  */
#define GFXDATA_OBJECTS_BRICK3           10       /* RLE  */
#define GFXDATA_OBJECTS_CROWN            11       /* RLE  */
#define GFXDATA_OBJECTS_ENERG            12       /* RLE  */
#define GFXDATA_OBJECTS_GOUD             13       /* RLE  */
#define GFXDATA_OBJECTS_HEKNEER          14       /* RLE  */
#define GFXDATA_OBJECTS_HEKOP            15       /* RLE  */
#define GFXDATA_OBJECTS_HOUWEEL          16       /* RLE  */
#define GFXDATA_OBJECTS_LINKS            17       /* RLE  */
#define GFXDATA_OBJECTS_MIJTER           18       /* RLE  */
#define GFXDATA_OBJECTS_MINE             19       /* RLE  */
#define GFXDATA_OBJECTS_NELI             20       /* RLE  */
#define GFXDATA_OBJECTS_NERE             21       /* RLE  */
#define GFXDATA_OBJECTS_NOBRICKS         22       /* RLE  */
#define GFXDATA_OBJECTS_ONLYBRICKS       23       /* RLE  */
#define GFXDATA_OBJECTS_PSTUT            24       /* RLE  */
#define GFXDATA_OBJECTS_RAND             25       /* RLE  */
#define GFXDATA_OBJECTS_RECHTS           26       /* RLE  */
#define GFXDATA_OBJECTS_SCHEDEL          27       /* RLE  */
#define GFXDATA_OBJECTS_STER             28       /* RLE  */
#define GFXDATA_OBJECTS_STUT             29       /* RLE  */
#define GFXDATA_OBJECTS_COUNT            30

#define GFXDATA_OTHER                    3        /* FILE */

#define GFXDATA_OTHER_BOARD              0        /* BMP  */
#define GFXDATA_OTHER_STELLER            1        /* BMP  */
#define GFXDATA_OTHER_TELLER             2        /* BMP  */
#define GFXDATA_OTHER_TITLE_BACK         3        /* BMP  */
#define GFXDATA_OTHER_COUNT              4

#define GFXDATA_PAL                      4        /* PAL  */
#define GFXDATA_COUNT                    5

#define SNDDATA                          1        /* FILE */

#define SNDDATA_AARG                     0        /* SAMP */
#define SNDDATA_BOOM                     1        /* SAMP */
#define SNDDATA_GGGG                     2        /* SAMP */
#define SNDDATA_GONG                     3        /* SAMP */
#define SNDDATA_GONG2                    4        /* SAMP */
#define SNDDATA_HAHA                     5        /* SAMP */
#define SNDDATA_KLOING                   6        /* SAMP */
#define SNDDATA_KNETTER                  7        /* SAMP */
#define SNDDATA_KRR                      8        /* SAMP */
#define SNDDATA_MOEH                     9        /* SAMP */
#define SNDDATA_PIEP                     10       /* SAMP */
#define SNDDATA_PIEUW                    11       /* SAMP */
#define SNDDATA_TIK                      12       /* SAMP */
#define SNDDATA_WOESH                    13       /* SAMP */
#define SNDDATA_ZZZJ                     14       /* SAMP */
#define SNDDATA_COUNT                    15


