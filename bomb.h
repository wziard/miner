#ifndef _BOMB_H_
#define _BOMB_H_

class BOMB
    : public STATIC_OBJECT
{
   public:
      BOMB();
      int proc();
      int can_coexist(int tag);
};


#endif
