#include "globals.h"
#include "gfxdata.h"
#include "objidx.h"
#include "ster.h"

STER::STER()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_STER].dat);
   tag = TAG_BONUS;
   passflags |= PF_PLAYER;
   score.stars = 1;
   name ="STER";
   idx = OBJ_STER;
}

int STER::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


