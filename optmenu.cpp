#include <math.h>
#include <stdio.h>
#include "optmenu.h"
#include "d_procs.h"
#include "globals.h"
#include "menu.h"
#include "init.h"

static DIALOG *the_options_dialog;

#define SPEED_SLIDER    2
#define SPEED_NUMBER    8
#define MAPSIZE_SLIDER  4
#define SOUND_SLIDER    6
#define SOUND_TEXT      7
#define MAPSIZE_NUMBER  9
#define SOUND_NUMBER    10

#define log2(x)     (log(x)/log(2))

void hide_all_tabs()
{
   the_options_dialog[SPEED_SLIDER].flags |= D_HIDDEN;
   the_options_dialog[SPEED_NUMBER].flags |= D_HIDDEN;
   the_options_dialog[MAPSIZE_SLIDER].flags |= D_HIDDEN;
   the_options_dialog[MAPSIZE_NUMBER].flags |= D_HIDDEN;
   
   the_options_dialog[SOUND_SLIDER].flags |= D_HIDDEN;
   the_options_dialog[SOUND_TEXT].flags |= D_HIDDEN;
   the_options_dialog[SOUND_NUMBER].flags |= D_HIDDEN;
}

static int d_gamespeed_proc(int msg, DIALOG *d, int c)
{
   int ret = d_seltext_proc(msg,d,c);
   if (msg == MSG_CLICK && (d->flags & D_SELECTED))
   {
      hide_all_tabs();
      the_options_dialog[SPEED_SLIDER].flags &= ~D_HIDDEN;
      the_options_dialog[SPEED_NUMBER].flags &= ~D_HIDDEN;
      return D_REDRAW;
   }

   return ret;
}

int speedslider_callback(void *dp3, int value)
{
   dp3 = NULL; // don't use
   the_options_dialog[SPEED_NUMBER].d2 = value + 32;
   return D_O_K;
}

static int d_mapsize_proc(int msg, DIALOG *d, int c)
{
   int ret = d_seltext_proc(msg,d,c);
   if (msg == MSG_CLICK && (d->flags & D_SELECTED))
   {
      hide_all_tabs();
      the_options_dialog[MAPSIZE_SLIDER].flags &= ~D_HIDDEN;
      the_options_dialog[MAPSIZE_NUMBER].flags &= ~D_HIDDEN;
      return D_REDRAW;
   }

   return ret;
}

int mapsizeslider_callback(void *dp3, int value)
{
   dp3 = NULL; // don't use
   the_options_dialog[MAPSIZE_NUMBER].d2 = value + 20;
   return D_O_K;
}


static int d_sound_proc(int msg, DIALOG *d, int c)
{
   int ret = d_seltext_proc(msg,d,c);
   if (msg == MSG_CLICK && (d->flags & D_SELECTED))
   {
      hide_all_tabs();
      the_options_dialog[SOUND_SLIDER].flags &= ~D_HIDDEN;
      the_options_dialog[SOUND_TEXT].flags &= ~D_HIDDEN;
      the_options_dialog[SOUND_NUMBER].flags &= ~D_HIDDEN;
      return D_REDRAW;
   }

   return ret;
}

static int d_back_proc(int msg, DIALOG *d, int c)
{
   int ret = d_seltext_proc(msg,d,c);
   if (msg == MSG_CLICK && (d->flags & D_SELECTED))
   {
      return D_EXIT;
   }

   return ret;
}



int soundslider_callback(void *dp3, int value)
{
   dp3 = NULL; // don't use
   the_options_dialog[SOUND_NUMBER].d2 = 1<<value;
   return D_O_K;
}


void options_menu()
{

   show_mouse(screen);
   DIALOG options_dialog[] =
   {
   /* (dialog proc)     (x)   (y)   (w)   (h)   (fg)  (bg)  (key) (flags)  (d1)  (d2)  (dp) */
      {d_bitmap_proc,     0,   0,SCREEN_W,SCREEN_H,0,255,     0,     0,      0,    0, title_back},
      {d_gamespeed_proc, 20,  10,    0,    0,    0,   0,      0,D_SELECTED,  0,    0, const_cast<char *>("gamespeed")},
      {d_slider_proc,   300,  20,   30,  440,    0, 255,      0,     0,    255,    get_config_int("MINER","BPS", 32) - 32, NULL,  (void *)speedslider_callback},
      {d_mapsize_proc,   20,  60,    0,    0,    0,   0,      0,     0,      0,    0, const_cast<char *>("mapsize")},
      {d_slider_proc,   300,  20,   30,  440,    0, 255,      0,D_HIDDEN,   80,    get_config_int("MINER","MAP_SIZE",20) - 20, NULL,  (void *)mapsizeslider_callback},
      {d_sound_proc,     20, 110,    0,    0,    0,   0,      0,     0,      0,    0, const_cast<char *>("sound")},
      {d_slider_proc,   300,  40,   30,  300,    0, 255,      0,D_HIDDEN,    6,  (int)log2(get_config_int("MINER","NR_OF_VOICES",0) + 1), NULL, (void *)soundslider_callback},
      {d_text_proc,     300,  10,  200,   30,    0, 255,      0,D_HIDDEN,    0,    0, const_cast<char *>("Nr of simultaneous sounds")},
      {d_number_proc,   330,  20,    0,    0,    0,   0,      0,     0,      3,   get_config_int("MINER","BPS", 32), NULL},
      {d_number_proc,   330,  20,    0,    0,    0,   0,      0,D_HIDDEN,    3,   get_config_int("MINER","MAP_SIZE",20) , NULL},
      {d_number_proc,   330,  40,    0,    0,    0,   0,      0,D_HIDDEN,    2,   get_config_int("MINER","NR_OF_VOICES",1), NULL},
      {d_back_proc,      20, 160,    0,    0,    0,   0,      0,     0,      0,    0, const_cast<char *>("exit menu")},
      {NULL,              0,   0,    0,    0,    0,   0,      0,     0,      0,    0,    0, NULL}
   };


   the_options_dialog = options_dialog; // ugly, but I found it easier to initialise the dialog
   // within the function, where all BITMAPS's are loaded etc.
   PALETTE options_palette;

   for (int i=0;i < 256;i++)
   {
      options_palette[i].r = game_palette[i].g;
      options_palette[i].g = game_palette[i].b;
      options_palette[i].b = game_palette[i].r;
   }
   set_dialog_color(options_dialog, bestfit_color(options_palette,0,0,0), bestfit_color(options_palette,63,63,63));


   DIALOG_PLAYER *p = init_dialog(options_dialog, 1);
   
   broadcast_dialog_message(MSG_DRAW, 0);
//   update_dialog(p);
   if (fade_speed)
   {
      fade_from(desktop_palette,options_palette,fade_speed);
   }
   else
   {
      set_palette(options_palette);
   }


   while(update_dialog(p));

   shutdown_dialog(p);

   set_config_int("MINER","BPS", options_dialog[SPEED_SLIDER].d2 + 32);
   set_config_int("MINER","MAP_SIZE", options_dialog[MAPSIZE_SLIDER].d2 + 20);
   set_config_int("MINER","NR_OF_VOICES", (1<<options_dialog[SOUND_SLIDER].d2));
   show_mouse(NULL);

   reinit();
   
   return;
}
