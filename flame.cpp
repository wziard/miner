#include "globals.h"
#include "gfxdata.h"
#include "flame.h"
#include "objidx.h"
#include "burn.h"



FLAME::FLAME()
   : ANIMATED_OBJECT((BITMAP *)anims[GFXDATA_ANIMS_FLAME].dat)
{
   tag = TAG_MONSTER;
   frame = 0;
   name = "FLAME";
   idx = OBJ_FLAME;
}

int FLAME::proc()
{
   OBJECT *here;

   switch(state)
   {

      case FLAME_STATE_PIEUW:
      if (!frame)
      {
             try_sample(SNDDATA_PIEUW, mapx,mapy);
   
      }
      if (frame == 31)
      {

         if ((here = playfield->contains_type(mapx,mapy,TAG_PLAYER)))
            here->die();
   
         state= FLAME_STATE_SLEEP;
            
         return F_CONTINUE;
      }
      break;
      case FLAME_STATE_SLEEP:

          if (frame ==  64)
          {
            if (playfield->contains_object(mapx,mapy,OBJ_GROEN) && !playfield->contains_object(mapx,mapy,OBJ_BURN))
                playfield->set(mapx,mapy,objcol->new_obj(OBJ_BURN));

            return F_DELETE;
          }
          
      break;
   }
   frame++;
   return F_CONTINUE;
}

int FLAME::can_coexist(int tag)
{
   tag = 0;
   return TRUE;
}
