#ifndef _OBJCOL_H_
#define _OBJCOL_H_

#include "objidx.h"
#include "objects.h"
#include "break.h"
#include "ne_li.h"
#include "hekop.h"
#include "hekneer.h"
#include "blokal.h"
#include "groen.h"
#include "energ.h"
#include "houweel.h"
#include "bomal.h"
#include "net.h"
#include "stut.h"
#include "rtile.h"
#include "objects.h"
#include "box.h"
#include "balloon.h"
#include "links.h"
#include "rechts.h"
#include "mine.h"
#include "brick.h"
#include "backg.h"
#include "flame.h"
#include "ne_re.h"
#include "ster.h"
#include "goud.h"
#include "pstut.h"
#include "crown.h"
#include "schedel.h"
#include "boem.h"
#include "bomb.h"
#include "man.h"
#include "burn.h"
#include "monster.h"


class OBJECT_COLLECTION
{

   public:
      OBJECT_COLLECTION();
      ~OBJECT_COLLECTION();

      OBJECT *new_obj(int idx, int extra = 0, int extra2 = 0);
      void del(OBJECT *o);

   private:

      HEAD *collection;
      OBJECT *really_new(int idx,int extra1, int extra2);

};






#endif
