#ifndef _MINE_H_
#define _MINE_H_

#include "objects.h"


#define MINE_STATE_STANDING 0
#define MINE_STATE_FALLING 1

class MINE
    : public STATIC_OBJECT
{
  public:
   MINE();
   int proc();
  private:
   int speed;
};

#endif
