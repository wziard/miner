#ifndef _SND_H_
#define _SND_H_
#include "allegr.h"

void try_loop(short *voice, int sample_index, int x, int y, int volume = 255, int priority = 128, int freq = 1000);
void try_loop_backwards(short *voice, int sample_index, int x, int y, int volume = 255, int priority = 128, int freq = 1000);
void try_sample(int sample_index, int x, int y, int volume = 255, int priority = 128, int freq = 1000);
void try_sample_backwards(int sample_index, int x, int y, int volume = 255, int priority = 128, int freq = 1000);
void kill_loop(short *voice);

#endif


