#ifndef _EFFECT_H_
#define _EFFECT_H_
#include <allegro.h>

extern void init_effect_tables(int w, int h);
extern void effect(BITMAP *orig, int duration, int offx, int offy);
extern void cleanup_effect_tables();


#endif
