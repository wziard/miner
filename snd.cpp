#include "snd.h"
#include "globals.h"
#include "map.h"

void try_loop(short *voice, int sample_index, int x, int y, int volume, int priority, int freq)
{
    if (!snd_data)
       return;

    SAMPLE *s = (SAMPLE *)(snd_data[sample_index].dat);

    if (*voice <0)
    {
        *voice = allocate_voice(s);
         if (*voice >= 0)
         {
            voice_start(*voice);
            voice_set_frequency(*voice, voice_get_frequency(*voice) * freq / 1000);
         }
    }

    if (playfield && !playfield->visible(x,y))
    {
      volume /= 2;
      priority /= 2;
    }
    
    if (*voice >=0)
    {
         voice_set_volume(*voice,volume);
         voice_set_priority(*voice,priority);
         voice_set_playmode(*voice, PLAYMODE_LOOP);
    }

}

void try_loop_backwards(short *voice, int sample_index, int x, int y, int volume, int priority, int freq)
{
    if (!snd_data)
       return;

    SAMPLE *s = (SAMPLE *)(snd_data[sample_index].dat);

    if (*voice <0)
    {
        *voice = allocate_voice(s);
         if (*voice >= 0)
         {
            voice_start(*voice);
            voice_set_frequency(*voice, voice_get_frequency(*voice) * freq / 1000);
            voice_set_playmode(*voice, PLAYMODE_LOOP | PLAYMODE_BACKWARD);
         }
    }

    if (playfield && !playfield->visible(x,y))
    {
      volume /= 2;
      priority /= 2;
    }
    
    if (*voice >=0)
    {
         voice_set_volume(*voice,volume);
         voice_set_priority(*voice,priority);
    }

}

void try_sample(int sample_index, int x, int y, int volume, int priority, int freq)
{
    if (!snd_data)
       return;

    SAMPLE *s = (SAMPLE *)(snd_data[sample_index].dat);
    int voice = allocate_voice(s);

    if (playfield && !playfield->visible(x,y))
    {
      volume /= 2;
      priority /= 2;
    }
    
    if (voice >=0)
    {
         voice_set_volume(voice,volume);
         voice_set_priority(voice,priority);
         voice_set_frequency(voice, voice_get_frequency(voice) * freq / 1000);
         voice_start(voice);
         voice_set_playmode(voice, PLAYMODE_PLAY);
         release_voice(voice);
    }


}

void try_sample_backwards(int sample_index, int x, int y, int volume, int priority, int freq)
{
    if (!snd_data)
       return;

    SAMPLE *s = (SAMPLE *)(snd_data[sample_index].dat);
    int voice = allocate_voice(s);

    if (!playfield->visible(x,y))
    {
      volume /= 2;
      priority /= 2;
    }
    
    if (voice >=0)
    {
         voice_set_volume(voice,volume);
         voice_set_priority(voice,priority);
         voice_set_frequency(voice, voice_get_frequency(voice) * freq / 1000);
         voice_set_playmode(voice, PLAYMODE_BACKWARD);
         voice_start(voice);
         release_voice(voice);
    }


}


void kill_loop(short *voice)
{
 if (!snd_data)
   return;

 if (*voice < 0)
   return;

 voice_stop(*voice);
 deallocate_voice(*voice);
 *voice = -1;
}


