#include "globals.h"
#include "data.h"
#include "bomal.h"
#include "setje.h"
#include "boem.h"
#include "explode.h"
#include "objidx.h"

BOMAL::BOMAL()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BOMAL].dat);   
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = BOMAL_STATE_STANDING;
   speed = 1;
   name = "BOMAL";
   idx = OBJ_BOMAL;
}

int BOMAL::proc()
{
   frame++;
   frame %= (32 / speed);
   switch(state)
   {

      case BOMAL_STATE_FALLING:
        if (frame == (32 / speed) - 1)
        {
            move(0,1);

            if (!playfield->allowed_move(mapx ,mapy+1,PF_MONSTER))
            {
               state = BOMAL_STATE_STANDING;
               explode(mapx,mapy);
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;

        break;

      case BOMAL_STATE_RISING:
        if (frame == (32 / speed) - 1)
        {
            move(0,-1);
            if (!playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
            {
               state = BOMAL_STATE_STANDING;
               explode(mapx,mapy);
               return F_DELETE;
            }
               
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs-=speed;
        }
        return F_CONTINUE;
        break;

      case BOMAL_STATE_LINKS:
        if (frame == (32 / speed) - 1)
        {
            move(-1,0);

            if (!playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
            {
               state = BOMAL_STATE_STANDING;
               explode(mapx,mapy);
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            xoffs-=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;

      case BOMAL_STATE_RECHTS:
        if (frame == (32 / speed) - 1)
        {
            move(1,0);

            if (!playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
            {
               state = BOMAL_STATE_STANDING;
               explode(mapx,mapy);
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               
            }
        }
        else
        {
            xoffs+=speed;
        }
        return F_CONTINUE;
        break;


      default:
        if (playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
        {
            state = BOMAL_STATE_LINKS;
            frame = 0;
            speed = 1;
            playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
        {
            state = BOMAL_STATE_RECHTS;
            frame = 0;
            speed = 1;
            playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
            
        }
        if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
        {
            state = BOMAL_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
        {
            state = BOMAL_STATE_RISING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}


