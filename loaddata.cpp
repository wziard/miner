#include "data.h"
#include "gfxdata.h"
#include "snddata.h"
#include "loaddata.h"
#include "globals.h"
#include "helpers.h"


static DATAFILE *gdata = NULL;
static DATAFILE *sdata = NULL;

void load_gfx_data()
{
   RGB *tmppal;
   DATAFILE *paldata;
   if (get_config_int("MINER","APPENDED_DATA",0))
   {
      paldata =load_datafile_object("#","GFXDATA/GFXDATA_PAL");

      if (!paldata)
         printerror("could not load pallete from appended datafile");

      tmppal = (RGB *) paldata->dat;
      tmppal[0].r = 63;
      tmppal[0].g = 0;
      tmppal[0].b = 63;

      select_palette(tmppal);

      gdata = load_datafile_object("#","GFXDATA");
   }
   else
   {
      paldata =load_datafile_object("data.dat","GFXDATA/PAL");

      if (!paldata)
         printerror("could not load pallete from data.dat");

      tmppal = (RGB *) paldata->dat;

      tmppal[0].r = 63;
      tmppal[0].g = 0;
      tmppal[0].b = 63;
      
      select_palette(tmppal);
      gdata = load_datafile_object("data.dat","GFXDATA");
   }
   if (!gdata)
   {
      fprintf(stderr,"could not load graphics data\n");
      fprintf(stderr,"allegro_error:'%s'\n",allegro_error);
      
      exit(1);
   }
   gfx_data = (DATAFILE *)(gdata->dat);

   anims = (DATAFILE *)gfx_data[GFXDATA_ANIMS].dat;
   objects = (DATAFILE *)gfx_data[GFXDATA_OBJECTS].dat;
   if (tight_mem)
   {
      fnt_andrus = font;
      fnt_andrus_4 = font;
      fnt_sml = font;
   }
   else
   {
      fnt_andrus = (FONT *)(((DATAFILE*)(gfx_data[GFXDATA_FONTS].dat))[GFXDATA_FONTS_ANDRUS].dat);
      fnt_andrus_4 = (FONT *)(((DATAFILE*)(gfx_data[GFXDATA_FONTS].dat))[GFXDATA_FONTS_ANDRUS_4].dat);
      fnt_sml = (FONT *)(((DATAFILE*)(gfx_data[GFXDATA_FONTS].dat))[GFXDATA_FONTS_CAROL].dat);
   }
   title_back = (BITMAP *)(((DATAFILE*)(gfx_data[GFXDATA_OTHER].dat))[GFXDATA_OTHER_TITLE_BACK].dat);
   board =  (BITMAP *)(((DATAFILE*)(gfx_data[GFXDATA_OTHER].dat))[GFXDATA_OTHER_BOARD].dat);
}


void load_snd_data()
{

   if (digi_driver->id == DIGI_NONE)
   {
      sdata= NULL;
      return;
   }
   if (get_config_int("MINER","APPENDED_DATA",0))
   {
      sdata = load_datafile_object("#","SNDDATA");
   }
   else
   {
      sdata = load_datafile_object("data.dat","SNDDATA");
   }
   if (sdata)
      snd_data = (DATAFILE *)(sdata->dat);
}


void unload_data()
{
   unload_datafile_object(gdata);

   if (sdata)
      unload_datafile_object(sdata);
   gfx_data = NULL;
   snd_data = NULL;
   anims = NULL;
   objects =  NULL;
}
