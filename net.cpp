#include "net.h"
#include "stut.h"
#include "gfxdata.h"
#include "objidx.h"
#include "globals.h"

NET::NET(int initial_state, int _generation)
          : COMPLEX_OBJECT()
{
   state = initial_state;
   generation = _generation;
   create();
}

NET::NET(int _generation)
          : COMPLEX_OBJECT()
{
   state = NET_STATE_STAND;
   generation = _generation;
   create();
}


NET::~NET()
{
}

void NET::create()
{
   name = "NET";
   idx = OBJ_NET;
   if (!(gfx_objects_table->gfx_objects[idx]))
   {

      gfx_objects_table->init_object(idx, 5);
      gfx_objects_table->set_object_state(idx,NET_STATE_STAND,(BITMAP *)(anims[GFXDATA_ANIMS_NET].dat));
      gfx_objects_table->set_object_state(idx,NET_STATE_RIGHT,(BITMAP *)(anims[GFXDATA_ANIMS_NETR].dat));
      gfx_objects_table->set_object_state(idx,NET_STATE_LEFT,(BITMAP *)(anims[GFXDATA_ANIMS_NETL].dat));
      gfx_objects_table->set_object_state(idx,NET_STATE_UP,(BITMAP *)(anims[GFXDATA_ANIMS_NETU].dat));
      gfx_objects_table->set_object_state(idx,NET_STATE_DOWN,(BITMAP *)(anims[GFXDATA_ANIMS_NETD].dat));
   }
   gfx_object = gfx_objects_table->gfx_objects[idx];
   passflags = PF_PLAYER;
   passflags |= PF_SMARTMONSTER;
   tag = TAG_BONUS;
   name = "NET";
   my_layer = 1;
   score.points = 100;
}

int NET::proc()
{
   frame++;
   frame %=32;

   switch(state)
   {
      case NET_STATE_UP:
      case NET_STATE_DOWN:
      case NET_STATE_LEFT:
      case NET_STATE_RIGHT:

       if (frame == 1)
          try_sample(SNDDATA_HAHA, mapx,mapy, 255,(generation*10));

       if (frame == 31)
          state = NET_STATE_STAND;
       break;
      default:
        if (generation > 0)
        {
         if (playfield->is_empty(mapx,mapy - 1))
            playfield->set(mapx, mapy - 1, objcol->new_obj(OBJ_NET, NET_STATE_UP,generation -1));

         if (playfield->is_empty(mapx,mapy + 1))
            playfield->set(mapx, mapy + 1, objcol->new_obj(OBJ_NET, NET_STATE_DOWN,generation -1));
      
         if (playfield->is_empty(mapx - 1,mapy ))
            playfield->set(mapx - 1, mapy, objcol->new_obj(OBJ_NET, NET_STATE_LEFT,generation -1));

         if (playfield->is_empty(mapx + 1,mapy))
            playfield->set(mapx + 1, mapy, objcol->new_obj(OBJ_NET, NET_STATE_RIGHT,generation -1));
        }
        generation = 0;
        return F_DONE;
        break;
   }

   return F_CONTINUE;
}


void NET::set_state(int _state,int _gen)
{
 generation = _gen;
 state = _state;
 must_be_deleted = 0;
 frame = 0;
}
