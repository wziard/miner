#include "globals.h"
#include "gfxdata.h"
#include "break.h"
#include "setje.h"
#include "objidx.h"



BREAK::BREAK()
   : ANIMATED_OBJECT((BITMAP *)anims[GFXDATA_ANIMS_BREAK].dat)
{
   tag = TAG_MONSTER;
   frame = 0;
   name = "BREAK";
   idx = OBJ_BREAK;
}

int BREAK::proc()
{
   if (frame == 31)
   {
      playfield->set(mapx,mapy,objcol->new_obj(get_random_object(0)));
      return F_DELETE;
   }
   frame++;
   return F_CONTINUE;
}


