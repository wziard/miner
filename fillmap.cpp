#include "fillmap.h"
#include "globals.h"
#include "helpers.h"
#include "objcol.h"



static void setrandom(OBJECT *o)
{
   int x,y;
   do
   {
     x = rand() % (playfield->w - 10);
     y = rand() % (playfield->h - 11);

     x+=5;
     y+=5;
     
   } while (!playfield->is_empty(x,y));
   playfield->set(x,y,o);
}

void fillmap()
{
   FUNCTIONLIST *tmp;
   OBJECT *o;


   tmp = proclist;
   proclist = NULL;
   
   
   for (int i=0;i<playfield->w;i++)
   {
      o = objcol->new_obj(OBJ_RTILE);
      if (!o)
         out_of_mem("creating border");

      playfield->set(i,0,o);

      o = objcol->new_obj(OBJ_RTILE);
      if (!o)
         out_of_mem("creating border");

      playfield->set(i, playfield->h - 1,o);
   }

   for (int i=1;i<playfield->h - 1;i++)
   {
      o = objcol->new_obj(OBJ_RTILE);
      if (!o)
         out_of_mem("creating border");

      playfield->set(0,i,o);

      o = objcol->new_obj(OBJ_RTILE);
      if (!o)
         out_of_mem("creating border");

      playfield->set(playfield->w - 1,i,o);
   }

   if (get_config_int("MINER","background",0))
   {
    for (int i=1;i<playfield->w -1 ;i++)
       for (int j=1;j<playfield->w -1 ;j++)
            playfield->set(i, j, objcol->new_obj(OBJ_BACKG));
   }
   
//   for (int i=0;i<1695;i++)
//      setrandom(objcol->new_obj(OBJ_BOMAL));

   playfield->set(2,2,objcol->new_obj(OBJ_STUT));
   int placable_size = (playfield->w -10)* (playfield->h-11);
//   fprintf(stderr,"size %d\n",placable_size);

//   for (int i=0;i<(placable_size/400+1) ;i++)
   for (int i=0;i<(placable_size/60+1) ;i++)
   {
      setrandom(objcol->new_obj(OBJ_BOMAL));
      setrandom(objcol->new_obj(OBJ_BOMB));
      setrandom(objcol->new_obj(OBJ_ENERG));
      setrandom(objcol->new_obj(OBJ_PSTUT));
      setrandom(objcol->new_obj(OBJ_MONSTER));
   }
   placable_size -= (placable_size/400 + 1) * 5 ;
//   fprintf(stderr,"bombs set size %d\n",placable_size);
   placable_size /= 20;
//   placable_size /=2;

//   fprintf(stderr,"loop size %d\n",placable_size);
   for (int i=0;i<placable_size;i++)
   {
      setrandom(objcol->new_obj(OBJ_BOX));
      setrandom(objcol->new_obj(OBJ_BOX));
      setrandom(objcol->new_obj(OBJ_BALLOON));
      setrandom(objcol->new_obj(OBJ_LINKS));
      setrandom(objcol->new_obj(OBJ_RECHTS));
      setrandom(objcol->new_obj(OBJ_MINE));
      setrandom(objcol->new_obj(OBJ_NE_RE));
      setrandom(objcol->new_obj(OBJ_STER));
      setrandom(objcol->new_obj(OBJ_GOUD));
      setrandom(objcol->new_obj(OBJ_SCHEDEL));
      setrandom(objcol->new_obj(OBJ_NE_LI));
      setrandom(objcol->new_obj(OBJ_HEKOP));
      setrandom(objcol->new_obj(OBJ_HEKNEER));
      setrandom(objcol->new_obj(OBJ_BLOKAL));
      setrandom(objcol->new_obj(OBJ_ENERG));

   }

         
   for (int i=1;i<playfield->w -1 ;i++)
       for (int j=1;j<playfield->w -1 ;j++)
         if (playfield->is_empty(i,j))
         {
            if (i > playfield->w/4 && i < playfield->w*3/4 && j > playfield->h/4 && j < playfield->h*3/4 )
                playfield->set(i, j, objcol->new_obj(OBJ_BRICK2));
            else
                playfield->set(i, j, objcol->new_obj(OBJ_BRICK));
         }


   proclist = tmp;
//  playfield->set(10,2, objcol->new_obj(OBJ_GROEN));
//  playfield->set(12,2, objcol->new_obj(OBJ_MONSTER));
//  playfield->set(12,2, objcol->new_obj(OBJ_CROWN));
}
