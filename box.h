#ifndef _BOX_H_
#define _BOX_H_

#include "objects.h"


#define BOX_STATE_STANDING 0
#define BOX_STATE_FALLING 1

class BOX
    : public STATIC_OBJECT
{
  public:
   BOX();
   int proc();
  private:
   int speed;
};

#endif
