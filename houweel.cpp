#include "globals.h"
#include "gfxdata.h"
#include "houweel.h"
#include "objidx.h"

HOUWEEL::HOUWEEL()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_HOUWEEL].dat);
   tag = TAG_BONUS;
   passflags |= PF_PLAYER;
   score.houws = 2;
   name ="HOUWEEL";
   idx = OBJ_HOUWEEL;
}

int HOUWEEL::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


