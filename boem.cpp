#include "globals.h"
#include "data.h"
#include "boem.h"
#include "objidx.h"




BOEM::BOEM()
   : ANIMATED_OBJECT((BITMAP *)anims[GFXDATA_ANIMS_BOEM].dat)
{
   tag = TAG_MONSTER;
   frame = 0;
   name = "BOEM";
   idx = OBJ_BOEM;
}

int BOEM::proc()
{
   
   if (frame == 31)
   {
      return F_DELETE;
   }
   frame++;
   return F_CONTINUE;
}


int BOEM::can_coexist(int tag)
{
   if (OBJECT::can_coexist(tag))
      return TRUE;

   if (tag == TAG_PLAYER) // never blowup player
      return TRUE;
      
   return FALSE;
}

