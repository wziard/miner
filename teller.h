#ifndef _TELLER_H_
#define _TELLER_H_

#include "allegr.h"

class TELLER
{
 public:
   TELLER(int nr_of_digits);
   ~TELLER();

   void set(int nr); // set it to this value
   void turn(int nr);// turn to this value

   
   void proc(); // should be called for each game logic frame
   void draw(BITMAP *onto, int x, int y); // draws current state
   void update(BITMAP *onto, int x, int y); // draws difference with last state
   
 private:
   void skip(int nr);// jump to this value and turn from there till final value is reached
   int current_value;
   int final_value;
   int now_goingto;
   int nr_of_digits;
   int *frame;
   int *running;
   int *dirty;
   void draw_frame(int frame, BITMAP *onto, int x, int y);
   int global_frame;
   int run_multiplier;
   BITMAP *digits;
};

#endif


