#include "globals.h"
#include "gfxdata.h"
#include "hekneer.h"
#include "objidx.h"

HEKNEER::HEKNEER()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_HEKNEER].dat);   
   tag = TAG_MONSTER;// though this is a monster it uses PF_STRUCTURE when walking
   passflags = PF_AXER;
   state = HEKNEER_STATE_STANDING;
   speed = 1;
   name = "HEKNEER";
   idx = OBJ_HEKNEER;
}

int HEKNEER::proc()
{

   frame++;
   frame %= (32 / speed);
   switch(state)
   {
      case HEKNEER_STATE_SLEEPING:
         if (frame == 31)
            state = HEKNEER_STATE_STANDING;
            
            return F_CONTINUE;
         break;
      case HEKNEER_STATE_FALLING:
        try_loop(&voice,SNDDATA_KRR, mapx,mapy);
          
        if (frame == (32 / speed) - 1)
        {
            move(0,1);
            playfield->set (mapx,mapy-1, objcol->new_obj(OBJ_HEKNEER, HEKNEER_STATE_SLEEPING));
            if (!playfield->allowed_move(mapx,mapy+1,PF_STRUCTURE))
            {
               kill_loop(&voice);
               state = HEKNEER_STATE_STANDING;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        yoffs = 0;
        if (playfield->allowed_move(mapx,mapy+1,PF_STRUCTURE))
        {
            state = HEKNEER_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->contains_type(mapx,mapy-1,TAG_PLAYER))
            passflags |= PF_PLAYER;
        else
            passflags &= ~PF_PLAYER;
        
        break;
   }
   return F_DONE;
}

int HEKNEER::can_coexist(int other)
{

   if (other == TAG_STRUCTURE)
      return FALSE;

   return OBJECT::can_coexist(other);

}
