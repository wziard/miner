#include "globals.h"
#include "data.h"
#include "snddata.h"
#include "box.h"
#include "break.h"
#include "objidx.h"

BOX::BOX()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BOX].dat);   
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = BOX_STATE_STANDING;
   speed = 1;
   name = "BOX";
   idx = OBJ_BOX;
}

int BOX::proc()
{
   OBJECT *collide_monster;
   frame++;
   frame %= (32 / speed);
   switch(state)
   {

      case BOX_STATE_FALLING:

      
        if (frame == (32 / speed) - 1)
        {
            move(0,1);

            if (!playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
            {
               kill_loop(&voice);
               state = BOX_STATE_STANDING;
               if (speed >4)
               {
                  collide_monster = playfield->contains_type(mapx,mapy+1, TAG_SMARTMONSTER);
                  if (collide_monster)
                     collide_monster->die();
                  
                  playfield->set(mapx,mapy, objcol->new_obj(OBJ_BREAK));
                  return F_DELETE;
               }
            }
            else
            {
               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
        {
            state = BOX_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            try_sample_backwards(SNDDATA_WOESH,mapx, mapy, 200);
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
