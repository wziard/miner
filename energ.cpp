#include "globals.h"
#include "gfxdata.h"
#include "energ.h"
#include "objidx.h"

ENERG::ENERG()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_ENERG].dat);
   tag = TAG_BONUS;
   passflags |= PF_PLAYER;
   score.energy = 100;
   name ="ENERG";
   idx = OBJ_ENERG;
}

int ENERG::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


