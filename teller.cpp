#include "teller.h"
#include "gfxdata.h"
#include "globals.h"
#include <string.h>

TELLER::TELLER(int nr)
{
   nr_of_digits =nr;
   frame = (int *)malloc(nr_of_digits * sizeof(int));
   dirty = (int *)malloc(nr_of_digits * sizeof(int));
   running = (int *)malloc(nr_of_digits * sizeof(int));
   if (!frame || !dirty || !running)
      printerror("out of memory");


   for (int i=0;i<nr_of_digits;i++)
   {
      frame[i] = running[i] = 0;
      dirty[i] = -1;
   }
   now_goingto = current_value = final_value = global_frame = 0;
   if (tight_mem)
   {
      digits = (BITMAP *)(((DATAFILE *)(gfx_data[GFXDATA_OTHER].dat))[GFXDATA_OTHER_STELLER].dat);
   }
   else
   {
      digits = (BITMAP *)(((DATAFILE *)(gfx_data[GFXDATA_OTHER].dat))[GFXDATA_OTHER_TELLER].dat);
   }
   run_multiplier = 1;
}

TELLER::~TELLER()
{
   free(frame);
   free(running);
   free(dirty);
}

void TELLER::set(int nr)
{
   final_value = nr;
   skip(nr);
}

void TELLER::skip(int nr)
{
   char buffer[100];
   int len;
   current_value = nr;

   sprintf(buffer,"%d",nr);
   run_multiplier = 1;
   while((int)strlen(buffer) > nr_of_digits)
   {
      for (int i=0;buffer[i];i++)
          buffer[i] = buffer[i+1];
   }
   len =strlen(buffer);
   for (int i=0;i<len;i++)
   {
      frame[i] = (buffer[len -1 -i] - '0') * 32;
      dirty[i] = -1;
      running[i] = 0;
   }
   global_frame = 0;
}


void TELLER::turn(int nr)
{
  final_value = nr;
}


void TELLER::proc()
{
   if (now_goingto != current_value)
   {
      if (now_goingto > current_value)
      {
         if (now_goingto - current_value > 200)
                  skip(now_goingto - 200);
         running[0] = 1;
      }
      else
      {
         if (now_goingto - current_value < -200)
                  skip(now_goingto + 200);
         running[0] = -1;
      }
      if (!(frame[0] % 32))
      {
         run_multiplier = 1;
         if (abs(now_goingto - current_value) > 20)
            run_multiplier = 2;
         if (abs(now_goingto - current_value) > 40)
            run_multiplier = 4;
      }
      running[0] *= run_multiplier;
      
      for (int i=0;i<nr_of_digits;i++)
      {
         if (running[i])
         {
            if (running[i]>0)
                        frame[i]+=running[i];
            
            if (frame[i] >= 289 && i <nr_of_digits - 1 && running[i] >0 && frame[i]-running[i] < 289)
               running[i+1] = 1;

            if ((frame[i] == 0 || frame[i] - running[i] >320)&& i <nr_of_digits - 1 && running[i] < 0 )
                  running[i+1] = -1;

            if (running[i]<0)
                        frame[i]+=running[i];
                        
            if (!(frame[i] % 32))
               running[i] = FALSE;

            if (frame[i] <0)
               frame[i] += 320;

            dirty[i] = TRUE;
            frame[i] %=320;
         }
      }
   
      global_frame+= run_multiplier;
      global_frame%=32;

      if (!global_frame)
      {
         if (now_goingto > current_value)
                  current_value++;
         else
                  current_value--;

      }
   }

   if (!global_frame)
      now_goingto = final_value;

}

void TELLER::draw(BITMAP *onto,int x, int y)
{
   for (int i=0;i<nr_of_digits;i++)
   {
      draw_frame(frame[i], onto, x + 16 *(nr_of_digits -1 -i),y);
      dirty[i] = 0;
   }
}

void TELLER::update(BITMAP *onto,int x, int y)
{
   for (int i=0;i<nr_of_digits;i++)
   {

      if (dirty[i])
            draw_frame(frame[i], onto, x + 16 *(nr_of_digits -1 -i),y);

      dirty[i] = 0;
   }
}

void TELLER::draw_frame(int frame, BITMAP *onto, int x, int y)
{
   static BITMAP *curframe;

   if (tight_mem)
   {
      curframe = create_sub_bitmap(digits, (frame/16)*16,0,16,32);
   }
   else
   {
      curframe = create_sub_bitmap(digits, frame*16,0,16,32);
   }
   blit(curframe,onto,0,0,x,y,16,32);
   destroy_bitmap(curframe);


}
