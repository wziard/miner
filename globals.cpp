#include "globals.h"





DATAFILE *gfx_data = NULL;
DATAFILE *snd_data = NULL;
DATAFILE *anims = NULL;
DATAFILE *objects = NULL;
MAP *playfield = NULL;
DRAWLIST *drawlist = NULL;
FUNCTIONLIST *proclist = NULL;
OBJECT_COLLECTION *objcol = NULL;
//PLACEHOLDER *placeholder = NULL;

volatile unsigned int global_speed_regulator  = 0;

int draw_placeholder;
int BPS; // beats per second 32 beats/move
int FPS; // frames per second , machine dependant

TELLER *points1;
TELLER *stars1;
TELLER *stuts1;
TELLER *lives1;
TELLER *houws1;
TELLER *energ1;


PALETTE grayscale_palette;

FONT *fnt_andrus;
FONT *fnt_andrus_4;
FONT *fnt_sml;
BITMAP *title_back;
RGB *game_palette;

BITMAP *screen_buffer;
int fade_speed;
GFX_OBJECTS_TABLE *gfx_objects_table = NULL;
int tight_mem = FALSE; // if this is set to true, some flashy parts are simplified
int animate_mouse = FALSE; // this will make a funny mousecursor when true
BITMAP *mousecursor = NULL;
BITMAP *board = NULL;
