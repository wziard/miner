#ifndef _FLAME_H_
#define _FLAME_H_

#define FLAME_STATE_PIEUW 0
#define FLAME_STATE_SLEEP 1


class FLAME
    : public ANIMATED_OBJECT
{
  public:
   FLAME();
   int proc();
   int can_coexist(int tag);
};


#endif
