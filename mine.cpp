#include "globals.h"
#include "gfxdata.h"
#include "mine.h"
#include "man.h"
#include "flame.h"
#include "explode.h"
#include "stdio.h"
#include "objidx.h"


MINE::MINE()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_MINE].dat);
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = MINE_STATE_STANDING;
   speed = 1;
   name = "MINE";
   idx = OBJ_MINE;
}

int MINE::proc()
{
   OBJECT *player_above;
   OBJECT *monster_under;
   frame++;
   frame %= (32 / speed);
   FLAME *flame;
   
   switch(state)
   {

      case MINE_STATE_FALLING:
      
        if (frame == (32 / speed) - 1)
        {
            move(0,1);

            if (!playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
            {
               kill_loop(&voice);
               state = MINE_STATE_STANDING;
               if (speed >=4)
               {
                  if ((monster_under = playfield->contains_type(mapx,mapy+1,TAG_SMARTMONSTER)))
                  {
                     monster_under->die();
                     
                  }
               }
               if (speed > 4)
               {
                     explode(mapx,mapy,1);
                     return F_DELETE;
               }
            }
            else
            {
               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;
      default:
        player_above = playfield->contains_type(mapx,mapy-1,TAG_PLAYER);
        if (!player_above)
                player_above = playfield->contains_type(mapx,mapy-1,TAG_AXER);

        if (player_above && !playfield->contains_type(mapx,mapy-1,TAG_MONSTER))
        {
             flame = (FLAME *)objcol->new_obj(OBJ_FLAME);
             playfield->set(mapx,mapy-1,flame);
        }
        if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
        {
            state = MINE_STATE_FALLING;
            try_sample_backwards(SNDDATA_WOESH,mapx, mapy, 200);
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
