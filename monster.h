#ifndef _MONSTER_H_
#define _MONSTER_H_

#include "objects.h"

#define MONSTER_STATE_STAND 0
#define MONSTER_STATE_RIGHT 1
#define MONSTER_STATE_LEFT  2
#define MONSTER_STATE_UP    3
#define MONSTER_STATE_DOWN  4

#define MONSTER_LOOK_DEPTH 5

class MONSTER
    : public COMPLEX_OBJECT
{
   public:
          MONSTER();
          MONSTER(int initial_state);
          ~MONSTER();
          int proc();
          void die();
   private:
      void create();
      int my_frame;
};


#endif
