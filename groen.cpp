#include "groen.h"
//#include "stdio.h"
#include "stut.h"
#include "gfxdata.h"
#include "objidx.h"
#include "globals.h"

GROEN::GROEN(int initial_state)
          : COMPLEX_OBJECT()
{
   state = initial_state;
   create();
   
}

GROEN::GROEN()
          : COMPLEX_OBJECT()
{
   state = GROEN_STATE_STAND;
   create();
}


GROEN::~GROEN()
{
}

void GROEN::create()
{
   name = "GROEN";
   idx = OBJ_GROEN;
   if (!(gfx_objects_table->gfx_objects[idx]))
   {

      gfx_objects_table->init_object(idx, 5);
      gfx_objects_table->set_object_state(idx,GROEN_STATE_STAND,(BITMAP *)(anims[GFXDATA_ANIMS_BUSH].dat));
      gfx_objects_table->set_object_state(idx,GROEN_STATE_RIGHT,(BITMAP *)(anims[GFXDATA_ANIMS_BUSHR].dat));
      gfx_objects_table->set_object_state(idx,GROEN_STATE_LEFT,(BITMAP *)(anims[GFXDATA_ANIMS_BUSHL].dat));
      gfx_objects_table->set_object_state(idx,GROEN_STATE_UP,(BITMAP *)(anims[GFXDATA_ANIMS_BUSHO].dat));
      gfx_objects_table->set_object_state(idx,GROEN_STATE_DOWN,(BITMAP *)(anims[GFXDATA_ANIMS_BUSHN].dat));
   }
   gfx_object = gfx_objects_table->gfx_objects[idx];
   
   passflags |= PF_PLAYER;
   passflags |= PF_SMARTMONSTER;
   passflags |= PF_STRUCTURE;
   tag = TAG_STRUCTURE;
   name = "GROEN";
   my_layer = 1;
   my_frame = 0;
}

int GROEN::proc()
{
   my_frame++;
   my_frame %= 5;
   
   if (!my_frame)
      frame++;
   frame %=32;

   switch(state)
   {
      case GROEN_STATE_UP:
      case GROEN_STATE_DOWN:
      case GROEN_STATE_LEFT:
      case GROEN_STATE_RIGHT:
       try_loop(&voice,SNDDATA_KRR, mapx, mapy, 40,10 ,500);
       
       if (frame == 31)
       {
          kill_loop(&voice);
          state = GROEN_STATE_STAND;
       }
       break;
      default:
         if (playfield->is_empty(mapx,mapy - 1))
            playfield->set(mapx, mapy - 1, objcol->new_obj(OBJ_GROEN, GROEN_STATE_UP));

         if (playfield->is_empty(mapx,mapy + 1))
            playfield->set(mapx, mapy + 1, objcol->new_obj(OBJ_GROEN, GROEN_STATE_DOWN));
      
         if (playfield->is_empty(mapx - 1,mapy ))
            playfield->set(mapx - 1, mapy,objcol->new_obj(OBJ_GROEN, GROEN_STATE_LEFT));

         if (playfield->is_empty(mapx + 1,mapy))
            playfield->set(mapx + 1, mapy, objcol->new_obj(OBJ_GROEN, GROEN_STATE_RIGHT));

         return F_DONE;
         break;
   }

   return F_CONTINUE;
}


