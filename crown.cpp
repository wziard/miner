#include "globals.h"
#include "gfxdata.h"
#include "crown.h"
#include "objidx.h"

CROWN::CROWN()
{
   if (get_config_int("MINER","SINT",0))
      sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_MIJTER].dat);
   else
      sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_CROWN].dat);
   tag = TAG_BONUS;
   passflags |= PF_PLAYER;
   score.points = 500;
   name ="CROWN";
   idx = OBJ_CROWN;
}

int CROWN::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


