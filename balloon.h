#ifndef _BALLOON_H_
#define _BALLOON_H_

#include "objects.h"


#define BALLOON_STATE_STANDING 0
#define BALLOON_STATE_FALLING 1

class BALLOON
    : public STATIC_OBJECT
{
  public:
   BALLOON();
   int proc();
  private:
   int speed;
};

#endif
