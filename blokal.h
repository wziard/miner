#ifndef _BLOKAL_H_
#define _BLOKAL_H_

#include "objects.h"


#define BLOKAL_STATE_STANDING 0
#define BLOKAL_STATE_FALLING  1
#define BLOKAL_STATE_RISING   2
#define BLOKAL_STATE_LINKS    3
#define BLOKAL_STATE_RECHTS   4

class BLOKAL
    : public STATIC_OBJECT
{
  public:
   BLOKAL();
   int proc();
  private:
   int speed;
};

#endif
