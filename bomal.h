#ifndef _BOMAL_H_
#define _BOMAL_H_

#include "objects.h"


#define BOMAL_STATE_STANDING 0
#define BOMAL_STATE_FALLING  1
#define BOMAL_STATE_RISING   2
#define BOMAL_STATE_LINKS    3
#define BOMAL_STATE_RECHTS   4

class BOMAL
    : public STATIC_OBJECT
{
  public:
   BOMAL();
   int proc();
  private:
   int speed;
};

#endif
