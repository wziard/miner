#ifndef _HEKOP_H_
#define _HEKOP_H_

#include "objects.h"


#define HEKOP_STATE_STANDING 0
#define HEKOP_STATE_FALLING 1
#define HEKOP_STATE_SLEEPING 2

class HEKOP
    : public STATIC_OBJECT
{
  public:
   HEKOP();
   int proc();
  private:
   int speed;
   int can_coexist(int other);
};

#endif
