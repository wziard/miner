#include "globals.h"
#include "helpers.h"
#include "interfac.h"
#include "fillmap.h"
#include "effect.h"


void screenshot()
{
 BITMAP *temp = create_sub_bitmap(screen,0,0,SCREEN_W,SCREEN_H);
 PALETTE current_palette;
 
 if (!temp)
    return;
 get_palette(current_palette);

 save_bitmap("minshot.pcx", temp, current_palette);

 destroy_bitmap(temp);
 


}


void oneplayer_game()
{
   MAN *player1;
   BITMAP *map;
   int local_speed_regulator, framecount, beatcount;
   int scale;
   int map_size = get_config_int("MINER","MAP_SIZE",70);
   map_size = MAX(map_size,20);
   playfield = new MAP(map_size,map_size,screen_buffer->w / 32,screen_buffer->h/ 32);
   fillmap();
   points1= new TELLER(8); // create 8 digit counter
   stars1 = new TELLER(2);
   stuts1 = new TELLER(3);
   lives1 = new TELLER(2);
   houws1 = new TELLER(3);
   energ1 = new TELLER(5);

   player1 =(MAN *)objcol->new_obj(OBJ_MAN, MAN_STATE_STAND, 5);
   playfield->set(2,2,player1);

   scale = MIN(SCREEN_W/playfield->w, (SCREEN_H-96)/playfield->h);
   map = create_sub_bitmap(screen, (SCREEN_W - playfield->w*scale)/2,(SCREEN_H -96 - playfield->h*scale)/2,playfield->w*scale,playfield->h*scale);

   
   proclist->add(player1);
   
   // in the beginning all was null and void....
   initialize_oneplayer_screen();

   if (fade_speed)
      fade_in(game_palette,fade_speed);
   else
      set_palette(game_palette);
   
   
   global_speed_regulator = 0;
   FPS = framecount = beatcount = 0;
   // and then there was TIME!
   while(!player1->gameover())
   {
      if (key[KEY_PRTSCR])
      {
            while (key[KEY_PRTSCR]);
            screenshot();
      }
      if (key[KEY_ESC])
      {
            while (key[KEY_ESC]);
            player1->die();
      }
      // draw a frame
      framecount++;
      
      while(!global_speed_regulator)
      {
         rest(10);
      }

      if (key[KEY_M])
      {
         playfield->draw_map(map,scale);
         playfield->map_dirty = TRUE;
      }
      else
         draw_oneplayer_frame();

      // do max 33 logic steps for each frame (33 since this is 32 + 1 so you'll still see movement)
      local_speed_regulator = MIN(global_speed_regulator, 33);
      global_speed_regulator = 0;
      // catch up game logic
      while(local_speed_regulator-- >0)
      {
         proclist->do_procs();
         points1->proc();
         stars1->proc();
         stuts1->proc();
         lives1->proc();
         houws1->proc();
         energ1->proc();
         
         beatcount++;
         if (beatcount == BPS) // one second elapsed, roughly
         {
            FPS = framecount;
            beatcount = 0;
            framecount = 0;
         }
      }
      if (key[KEY_SPACE])
           playfield->center(player1->mapx,player1->mapy);

   }
   objcol->del(player1);

   destroy_bitmap(map);

   delete points1;
   delete stars1;
   delete stuts1;
   delete lives1;
   delete houws1;
   delete energ1;
   delete playfield;
   if (fade_speed)
   {
      if (bitmap_color_depth(screen) == 8)
      {
          fade_out(1);
      }
      else
          effect(screen , 20, 0, 0);
   }
   else
      set_palette(black_palette);
      
}
