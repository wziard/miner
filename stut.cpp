#include "globals.h"
#include "gfxdata.h"
#include "stut.h"
#include "objidx.h"

STUT::STUT()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_STUT].dat);
   tag = TAG_STRONGSTRUCTURE;
   passflags |= PF_PLAYER;
   passflags |= PF_SMARTMONSTER;
   idx = OBJ_STUT;
   name ="STUT";
}

int STUT::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

int STUT::can_coexist(int tag)
{
   tag=0;
   return TRUE;
}
