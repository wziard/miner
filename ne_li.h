#ifndef _NE_LI_H_
#define _NE_LI_H_

#include "objects.h"


#define NE_LI_STATE_STANDING 0
#define NE_LI_STATE_FALLING 1
#define NE_LI_STATE_SLIDING 2


class NE_LI
    : public STATIC_OBJECT
{
  public:
   NE_LI();
   int proc();
  private:
};

#endif
