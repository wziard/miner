#include "map.h"
#include "globals.h"
#include "objidx.h"

MAP::MAP(int _w, int _h,int vpw,int vph)
{
  //_MM_MARK();
   w = _w;
   h = _h;
   vp_xoffs = vp_yoffs = vp_x = vp_y = 0;
   vp_w = vpw;
   vp_h = vph;
   map_dirty =  FALSE;
  //_MM_MARK();

   the_map =(OBJECT ****)malloc(sizeof (OBJECT ***) * w);
  //_MM_MARK();

  //_MM_MARK();
   if (!the_map)
      out_of_mem("creating map");
  //_MM_MARK();

   for (int i=0;i<w;i++)
   {
       the_map[i] = (OBJECT ***)malloc( sizeof (OBJECT **) * h);
       if (!the_map[i])
       {
         for (int j=0;j<i;j++)
             free(the_map[i]);

         free(the_map);
         out_of_mem("creating map");
       }

       for (int j=0;j<h;j++)
       {
         the_map[i][j] = (OBJECT **)malloc( sizeof (OBJECT *) * MAX_OBJ_PERTILE);
         if ((!the_map[i][j]))
         {
            for (int k = 0;k<i;k++)
            {
               for (int l = 0;l <h ;l++)
                   free(the_map[k][l]);

               free(the_map[k]);
            }
            for (int k=0;k<j;k++)
                free(the_map[i][k]);
                
            free(the_map[i]);

            free(the_map);

            out_of_mem("creating map");

         }
         for (int k=0;k<MAX_OBJ_PERTILE;k++)
             the_map[i][j][k] = NULL;
       }
   }
  //_MM_MARK();
}

MAP::~MAP()
{
   for (int i=0;i<w;i++)
   {
       for (int j = 0;j< h;j++)
       {
         for (int k=0;k<MAX_OBJ_PERTILE;k++)
         if (the_map[i][j][k] && !the_map[i][j][k]->nodelete)
            objcol->del(the_map[i][j][k]);
            
         free(the_map[i][j]);
       }
       free(the_map[i]);
   }
   free(the_map);
}

Score MAP::set(int x, int y, OBJECT *obj)
{
   Score ret;
   if (x>=w || y >= h)
      printerror("object set out of map bounds");


   if (obj->idx == OBJ_UNDEFINED)
      printerror("undefined object : %s",obj->name);


   for (int i=0;i<MAX_OBJ_PERTILE;i++)
       if (the_map[x][y][i] == obj)
          return ret; // it was already there
          
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
      if (!(the_map[x][y][i]))
      {
         the_map[x][y][i] = obj;
         if (obj)
         {
          obj->mapx = x;
          obj->mapy = y;
         }
         ret = playfield->someone_arrived(x,y,obj);
         
         wakeup(x-1,y);
         wakeup(x+1,y);
         wakeup(x,y-1);
         wakeup(x,y+1);
         wakeup(x,y); // make sure evrybody here is awake too
         
         return ret;
      }
   }


   allegro_exit();

   for (int i=0;i<MAX_OBJ_PERTILE;i++)
       printf("%s\n",the_map[x][y][i]->name);
   printf("tile overfull\n");
   exit(1);
       
   printerror("tile overfull");

   return ret;
}


void MAP::del(int x, int y, OBJECT *obj)
{
   if (x>=w || y >= h || x < 0 || y < 0)
      printerror("object deleted out of map bounds");
      
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
      if (the_map[x][y][i] == obj)
      {
         the_map[x][y][i] = NULL;
         wakeup(x-1,y);
         wakeup(x+1,y);
         wakeup(x,y-1);
         wakeup(x,y+1);
         return;
      }
   }

}


void MAP::wakeup(int x,int y)
{

   if (x <0 || x >= w || y <0 || y >=h || !proclist)
      return;

   for (int i=0;i<MAX_OBJ_PERTILE;i++)
       if (the_map[x][y][i])
          proclist->add(the_map[x][y][i]); // blindly add it, it cannot be added twice
}

OBJECT *MAP::get(int x, int y, int nr)
{
   if (x>=w || y >= h || nr >= MAX_OBJ_PERTILE)
      printerror("object asked out of map bounds");
      
   return the_map[x][y][nr];
}

void MAP::draw(BITMAP * onto, int layer)
{
   int x,y;
//   fprintf(stderr," draw region -------------------\n");

   for (int i = -2;i<vp_w+2;i++)
   {
      for (int j = -2; j<vp_h+2;j++)
      {
         x = i + vp_x;
         y = j + vp_y;
         if (x>=0 && x<w && y>=0 && y<w)
         {
            for (int k=0;k<MAX_OBJ_PERTILE;k++)
            {
                if (the_map[x][y][k])
                {
//                  fprintf(stderr," %s \n",the_map[x][y][k]->name);
                  the_map[x][y][k]->draw(onto, i *TILESIZE_X + vp_xoffs,j*TILESIZE_Y + vp_yoffs,layer);
                }
            }
         }
      }
   }
}

void MAP::draw(BITMAP * onto,int x, int y, int layer)
{
   if (x < vp_x - 1 || y < vp_y - 1 || x > vp_x + vp_w || y > vp_y + vp_h)
      return;

   if (x>=0 && x<w && y>=0 && y<w)
   {
      for (int k=0;k<MAX_OBJ_PERTILE;k++)
      {
         if (the_map[x][y][k])
         {
//            fprintf(stderr," %s \n",the_map[x][y][k]->name);
            the_map[x][y][k]->draw(onto,(x - vp_x) * TILESIZE_X + vp_xoffs, (y - vp_y)*TILESIZE_Y + vp_yoffs,layer);
         }
      }
   }
}

int MAP::allowed_move(int x, int y, int flag)
{
   if (x <0 || y <0 || x >= w || y >= h)
      return FALSE;

   int passflags = PF_ALL;
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
      if (the_map[x][y][i])
         passflags &= the_map[x][y][i]->passflags;
   }

   return (flag & passflags);
   
}


void MAP::center(int x, int y)
{
   int ovpx = vp_x;
   int ovpy = vp_y;
   if (x >=0) vp_x = x - vp_w/2;
   if (y >=0) vp_y = y - vp_h/2;

   vp_x = MAX(0, vp_x);
   vp_y = MAX(0, vp_y);

   vp_x = MIN(w - vp_w, vp_x);
   vp_y = MIN(h - vp_h, vp_y);

   if (vp_x != ovpx || vp_y != ovpy)
      map_dirty = TRUE;
}

// someone arrived at this map square
// check if all objects can coexist
// the new arrived one always wins
// returns points arrivel gives
Score MAP::someone_arrived(int x, int y,OBJECT *obj)
{
   Score score;
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
      if (the_map[x][y][i] && !(obj == the_map[x][y][i]) && !the_map[x][y][i]->must_be_deleted)
         if (!obj->can_coexist(the_map[x][y][i]->tag))
         {
            score += the_map[x][y][i]->score;
            the_map[x][y][i]->must_be_deleted = TRUE; // do not dlete it here, since that can break the proclist
            if (proclist)
                proclist->add(the_map[x][y][i]);
         }
   }
   return score;
}


int MAP::is_empty(int x, int y)
{

   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
       if (the_map[x][y][i])
       {
         if (the_map[x][y][i]->tag != TAG_BACKGROUND)
            return FALSE;

         if ((the_map[x][y][i]->passflags & PF_ALL) != PF_ALL)
            return FALSE;
            
       }
   }

   return TRUE;
}


OBJECT *MAP::contains_type(int x, int y, int tag)
{
   if (x <0 || y <0 || x >=w || y >=h)
      return NULL;
   
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
     if (the_map[x][y][i] && the_map[x][y][i]->tag == tag)
        return the_map[x][y][i];
   }

   return NULL;

}

OBJECT *MAP::contains_object(int x, int y, int idx)
{
   if (x <0 || y <0 || x >=w || y >=h)
      return NULL;
      
   for (int i=0;i<MAX_OBJ_PERTILE;i++)
   {
     if (the_map[x][y][i] && the_map[x][y][i]->idx == idx)
        return the_map[x][y][i];
   }

   return NULL;

}


int MAP::visible(int x, int y)
{

   if (x >= vp_x && x < vp_x + vp_w && y >= vp_y && y <=vp_y + vp_h)
      return TRUE;


   return FALSE;

}


void MAP::draw_map(BITMAP *onto, int scale)
{
   int color;

   for (int x=0;x<w;x++)
   {
      for (int y=0;y<h;y++)
      {
         if (is_empty(x,y))
                  color = makecol(0,0,0);
         else
                  color = makecol(200,100,0);

         if (contains_object(x,y,OBJ_BRICK2))
                  color = makecol(200,130,0);

         if (contains_type(x,y,TAG_BONUS))
                  color = makecol(255,255,0);


         if (!allowed_move(x,y,PF_PLAYER))
                  color = makecol(100,0,0);

         if (contains_object(x,y,OBJ_RTILE))
                  color = makecol(200,255,255);
                  
         if (contains_object(x,y,OBJ_GROEN))
                  color = makecol(0,255,0);
                  
         if (contains_object(x,y,OBJ_BURN))
                  color = makecol(255,0,0);
                  
         if (contains_object(x,y,OBJ_NET))
                  color = makecol(0,0,255);

         if (contains_object(x,y,OBJ_MAN))
                  color = makecol(255,0,255);



         rectfill(onto, x* scale, y * scale , x * scale + scale -1 , y* scale + scale -1,color);
      }
   }


}
