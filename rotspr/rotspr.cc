



#include <allegro.h>






void rotbmp(BITMAP *bmp)
{

   BITMAP *tmp =create_bitmap(bmp->w,bmp->h);
   for (int i=0;i<bmp->w;i++)
   {
      for (int j=0;j<bmp->h;j++)
      {
         putpixel(tmp, i,j, getpixel(bmp,j, bmp-> h -i-1));
      }
   
   }

   blit(tmp,bmp,0,0,0,0,bmp->w,bmp->h);
   destroy_bitmap(tmp);
}



int main(int argc,char **argv)
{

   allegro_init();

   BITMAP *bmp = load_bitmap(argv[1], desktop_palette);

   BITMAP *tmp;

   for (int i=0;i<bmp->w / 32;i++)
   {
      tmp = create_sub_bitmap(bmp, i * 32, 0, 32, 32);
      for (int j=0;j< atoi(argv[2]);j++)
      {
         rotbmp(tmp);
      }

      destroy_bitmap(tmp);
   }


   save_bitmap(argv[3],bmp, desktop_palette);

}
