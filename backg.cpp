#include "globals.h"
#include "data.h"
#include "backg.h"
#include "objidx.h"



BASE_BACKGROUND::BASE_BACKGROUND()
{

   name = "BACKGND";
   idx = OBJ_BACKG;

}

void BASE_BACKGROUND::draw(BITMAP *onto, int x, int y, int layer)
{
   if (layer !=0)
     return;//only draw if layer =0
   draw_rle_sprite(onto, sprite, x  + xoffs, y + yoffs);
}


BACKGROUND::BACKGROUND()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BACKGROUND2].dat);
   tag = TAG_BACKGROUND;
   passflags |= PF_ALL;
   score.points = 0;
}

int BACKGROUND::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

BACKGROUND2::BACKGROUND2()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BACKGROUND2].dat);
   tag = TAG_BACKGROUND;
   passflags |= PF_ALL;
   score.points = 0;
}

int BACKGROUND2::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

BACKGROUND3::BACKGROUND3()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BACKGROUND].dat);
   tag = TAG_BACKGROUND;
   passflags |= PF_ALL;
   score.points = 0;
}

int BACKGROUND3::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}

