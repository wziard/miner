#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <stdio.h>
#include "allegr.h"
#include "list.h"
#include "map.h"
#include "objects.h"
#include "objcol.h"
#include "snd.h"
#include "teller.h"

#define TILESIZE_X 32
#define TILESIZE_Y 32
#define NR_OF_LAYERS 4

class OBJECT_COLLECTION;

extern DATAFILE *gfx_data;
extern DATAFILE *anims;
extern DATAFILE *objects;
extern DATAFILE *snd_data;
extern MAP *playfield;
extern DRAWLIST *drawlist;
extern FUNCTIONLIST *proclist;
extern OBJECT_COLLECTION *objcol;
//extern PLACEHOLDER *placeholder;
volatile extern unsigned int global_speed_regulator; // just counts on and on and on
extern int draw_placeholder;
extern int BPS;
extern int FPS;
extern TELLER *points1;
extern TELLER *stars1;
extern TELLER *stuts1;
extern TELLER *lives1;
extern TELLER *houws1;
extern TELLER *energ1;
extern PALETTE grayscale_palette;

extern FONT *fnt_andrus;
extern FONT *fnt_andrus_4;
extern FONT *fnt_sml;
extern BITMAP *title_back;
extern BITMAP *screen_buffer;
extern RGB *game_palette;
extern int fade_speed;
extern GFX_OBJECTS_TABLE *gfx_objects_table;
extern int tight_mem;
extern int animate_mouse;
extern BITMAP *mousecursor;
extern BITMAP *board;

#endif
