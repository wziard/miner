#ifndef _MAP_H_
#define _MAP_H_
#include "allegr.h"
#include "helpers.h"
#include "list.h"

#define MAX_OBJ_PERTILE 5


class MAP
{
   public:
      MAP(int width, int height, int vpw, int vph);
      ~MAP();
      Score set(int x, int y, OBJECT *obj);
      void del(int x, int y, OBJECT *obj);
      OBJECT *get(int x, int y, int nr);
      OBJECT *contains_type(int x,int y, int tag);
      OBJECT *contains_object(int x,int y, int idx);
      
      void draw(BITMAP *onto, int layer);
      void draw(BITMAP *onto,int x, int y, int layer);
      void center(int x, int y); // make x,y the center of the viewport
      int vp_x,vp_y,vp_w,vp_h,vp_xoffs,vp_yoffs;
      int w,h;
      int allowed_move(int x, int y, int flag);
      int map_dirty; // complete map needs redrawing
      int is_empty(int x, int y); // returns TRUE when the tile contains only objects of type TAG_BACKGROUND and with passflags PF_ALL
      int visible(int x, int y);

      void draw_map(BITMAP *onto, int scale);
      
   private:
      Score someone_arrived(int x, int y,OBJECT *obj);
      OBJECT ****the_map; // 1 tile can contin more objects
      void wakeup(int x, int y);
};


#endif
