#ifndef _HEKNEER_H_
#define _HEKNEER_H_

#include "objects.h"


#define HEKNEER_STATE_STANDING 0
#define HEKNEER_STATE_FALLING 1
#define HEKNEER_STATE_SLEEPING 2

class HEKNEER
    : public STATIC_OBJECT
{
  public:
   HEKNEER();
   int proc();
   int can_coexist(int other);
  private:
   int speed;
};

#endif
