#include <stdlib.h>
#include "effect.h"
#include "helpers.h"
#include <math.h>

static int **x, **y;
static int table_w, table_h;

static void fill_effect_tables(int **x, int **y, int w, int h)
{
   int i, j;
   for (j = 0; j < h ;j++)
   {
    for (i = 0; i < w; i++)
    {
     x[j][i] = ((int)(i  + 3 * sin(i/70.0) + 2 * cos(j / 50.0)) + w) % w;
     y[j][i] = ((int)(j  + 2 * cos(i/40.0) - 2 * sin(j / 50.0)) + h) % h;;

    }
      
   }
 
}


void init_effect_tables(int w, int h)
{
   int i;

   x = (int **)malloc(sizeof(int *) * h);
   y = (int **)malloc(sizeof(int *) * h);

   if (!x || !y)
      printerror(" Out of memory");

   for (i= 0; i < h ;i++)
   {
      x[i] = (int *)malloc(w * sizeof(int));
      y[i] = (int *)malloc(w * sizeof(int));

   if (!x[i] || !y[i])
      printerror("out of memory");
      
   }
   table_w = w;
   table_h = h;
   fill_effect_tables(x,y,w,h);
}


void effect(BITMAP *orig, int duration, int offx, int offy)
{
 int w,h, i, j, c = 0;
 BITMAP *b1,*b2, *tmp;
 short *line;
 int zigzag[] = {-5,-4,-3, -2, -1, 0, 1 , 2, 3, 4, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4};



 w = orig->w;
 h = orig->h;

 if (w > table_w || h > table_h)
    printerror("effect tables too small");

 b1 = create_bitmap(w, h);
 b2 = create_bitmap(w, h);

 if (!b1 || !b2)
    printerror("out of memory");


 c = 0;
 blit (orig, b1, 0,0,0,0, w, h);
 while(!keypressed() && (!duration || c < duration))
 {
 
//  if (!c)
//    blit (orig, b1, 0,0,0,0, w, h);
    
  c++;
  
  
  for (j = 0; j < h; j++)
  {
   line = (short *)(b2->line[(j + zigzag[c % 18] + h)   % h]);
   for (i = 0; i< w; i++)
   {
    line[i] = ((short *)(b1->line[y[j][i]]))[x[j][i]];
   }
  }

  blit (b2, screen ,0,0,offx,offy,w, h);



  tmp = b2;
  b2 = b1;
  b1 = tmp;
 }

 destroy_bitmap(b1);
 destroy_bitmap(b2);
 
}


void cleanup_effect_tables()
{
   for (int i= 0; i < table_h ;i++)
   {
      free(x[i]);
      free(y[i]);

      
   }
   free(x);
   free(y);

   table_w = -1;

}
