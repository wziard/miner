#include "interfac.h"
#include "globals.h"
#include "helpers.h"
#include "stdio.h"
#include "snddata.h"
#include "gfxdata.h"


static int stuts =0;
static int houws =0;
static int lives =0;
static int stars =0;
static int points =0;
static int energy =0;


void initialize_oneplayer_screen()
{
   clear(screen_buffer);
   clear(screen);
   for (int i=0;i< NR_OF_LAYERS;i++)
       playfield->draw(screen_buffer,i);
   if (screen_buffer->w == SCREEN_W)
   {
      blit(screen_buffer,screen,0,0,0,0,SCREEN_W,SCREEN_H- 96);
   }
   else
   {
      stretch_blit(screen_buffer,screen,0,0, screen_buffer->w, screen_buffer->h,0,0,SCREEN_W,SCREEN_H- 96);
   }

   BITMAP *frame;
   for (int i=0; i<SCREEN_W;i+=640)
   {
      blit(board, screen, 0, 0 ,i , SCREEN_H - 96, 640, 96);
   }

   draw_rle_sprite(screen, (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_GOUD].dat), 10, SCREEN_H - 86);
   draw_rle_sprite(screen, (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_ENERG].dat),10, SCREEN_H - 44);
   draw_rle_sprite(screen, (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_STER].dat),200, SCREEN_H - 86);
   draw_rle_sprite(screen, (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_STUT].dat),200, SCREEN_H - 44);
   frame = create_sub_bitmap((BITMAP *)(anims[GFXDATA_ANIMS_MANS].dat), 0,0,32,32);
   masked_blit(frame,screen,0,0,400,SCREEN_H - 86,32,32);
   destroy_bitmap(frame);
   draw_rle_sprite(screen, (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_HOUWEEL].dat),400, SCREEN_H - 44);
   
}


static void dirty_blit(int x, int y)
{
   if (screen_buffer->w == SCREEN_W)
   {
      blit(screen_buffer, screen, x * TILESIZE_X, y * TILESIZE_Y, x * TILESIZE_X, y * TILESIZE_Y,TILESIZE_X,TILESIZE_Y);
   }
   else
   {
      int scale = SCREEN_W / screen_buffer->w;
      int xfrom = x * TILESIZE_X;
      int yfrom = y * TILESIZE_Y;
      int wfrom = TILESIZE_X;
      int hfrom = TILESIZE_Y;
      if (xfrom + wfrom > screen_buffer->w)
      {
          wfrom = screen_buffer->w - xfrom;
      }
      if (yfrom + hfrom > screen_buffer->h)
      {
          hfrom = screen_buffer->h - yfrom;
      }
      if (wfrom > 0 && hfrom > 0)
      {
        stretch_blit(screen_buffer, screen, xfrom, yfrom, wfrom, hfrom, xfrom * scale, yfrom * scale,wfrom*scale,hfrom*scale);
      }
   }
}

void draw_oneplayer_frame()
{
//    if (FPS >30)
//       vsync();

   textprintf_centre_ex
   (
      screen,font,600,450,1,-1,
      "fps %d ",
      FPS
   );
      
   points1->update(screen, 42,SCREEN_H - 86);
   energ1->update(screen,  42,SCREEN_H - 44);
   
   stars1->update(screen, 232,SCREEN_H - 86);
   stuts1->update(screen, 232,SCREEN_H - 44);
   lives1->update(screen, 432,SCREEN_H - 86);
   houws1->update(screen, 432,SCREEN_H - 44);

   if (playfield->map_dirty)
   {
      clear(screen_buffer);
      for (int i=0;i< NR_OF_LAYERS;i++)
           playfield->draw(screen_buffer,i);

      stretch_blit(screen_buffer,screen,0,0, screen_buffer->w, screen_buffer->h, 0,0,SCREEN_W,SCREEN_H- 96);
      playfield->map_dirty = 0;
      return;
   }
   for (int i=-1;i< NR_OF_LAYERS;i++)
   {
       drawlist->draw(screen_buffer,i);
   }
   drawlist->for_all(dirty_blit);
   drawlist->reset();
   
}

void set_oneplayer_statusbar(int s,int h, int l,int s_get, int e,int p)
{
   stuts = s;
   houws = h;
   lives = l;
   stars = s_get;
   points = p;
   energy = e;
}

