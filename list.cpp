#include "list.h"
#include "helpers.h"
#include "globals.h"
#include "objidx.h"

//lin!#define NULL 0


OBJECT::OBJECT()
{
   next = last = NULL;
   passflags = PF_NONE_SHALL_PASS;
   xoffs = yoffs = 0;
   mapx = mapy = 0; // will be set by MAP's set function
   tag = TAG_MONSTER; // nealy everything is a monster (it only means it cannot coexist with player in one tile)
   must_be_deleted = FALSE;
   nodelete = FALSE;
   state = 0;

   name = "GENERIC OBJECT";
   idx =OBJ_UNDEFINED;
   id_count++;
   voice = -1;
   id = id_count;
}


int OBJECT::can_coexist(int other_tag)
{
   if (other_tag == TAG_BACKGROUND || other_tag == TAG_STRONGSTRUCTURE || tag == TAG_BACKGROUND)
      return TRUE;

   if (tag ==  TAG_SMARTMONSTER && other_tag == TAG_STRUCTURE)
      return TRUE;

   if (tag ==  TAG_PLAYER && other_tag == TAG_STRUCTURE)
      return TRUE;
   return FALSE;
}

short OBJECT::id_count = 0;

OBJECT::~OBJECT()
{
   if (last)
      last->next = next;

   if (next)
      next->last = last;

   last = next = NULL;

}



void OBJECT::set_state(int _state, int extra)
{
 extra = 0;
 state = _state;
 must_be_deleted = 0;
 frame = 0;
}

Score OBJECT::move(int dx, int dy)
{
   playfield->del(mapx,mapy,this);
   mapx+= dx;
   mapy+= dy;
   xoffs = yoffs = 0;
   return playfield->set(mapx,mapy,this);
}



void OBJECT::remove_from_list()
{
   if (last)
      last->next = next;

   if (next)
      next->last = last;
      
   last = next = NULL;
}


void OBJECT::die()
{
   must_be_deleted = TRUE;
}

void OBJECT::add_to_list(OBJECT *head)
{
   // if it is already in a list, return

   if (last)
      return;

   if (!head)
   {
      last = next = NULL;
      return;
   }

   next = head->next;
   last = head;
   head->next = this;
   if (next)
      next->last = this;
   

}


OBJECT *OBJECT::get_next()
{
   return next;
}


DRAWLIST::DRAWLIST(int _size)
{
   size = _size;
   the_list = (POSITION *)malloc(sizeof (POSITION) * size);
   if (!the_list)
      out_of_mem("creating drawlist");
   reset();
}

DRAWLIST::~DRAWLIST()
{
   free(the_list);
}

void DRAWLIST::reset()
{
   nr_of_items = 0;
}

void DRAWLIST::lowlevel_add(int x, int y)
{
   for (int i=nr_of_items;i;i--)
      if ((x == the_list[i-1].x) && (y == the_list[i-1].y))
         return;

   // check list size
   if (nr_of_items == size)
   {
      warn("too many objects for drawlist");
      return;
   }
   
   the_list[nr_of_items].x  = x;
   the_list[nr_of_items].y  = y;
   nr_of_items++;

}


void DRAWLIST::add(OBJECT *obj)
{

   lowlevel_add(obj->mapx, obj->mapy);
   lowlevel_add(obj->mapx+1, obj->mapy);
   lowlevel_add(obj->mapx, obj->mapy+1);
   lowlevel_add(obj->mapx-1, obj->mapy);
   lowlevel_add(obj->mapx, obj->mapy-1);

}


void DRAWLIST::draw(BITMAP *onto, int layer)
{
   int x,y;
   // if layer is -1 then erase everything
//  fprintf(stderr,"draw #################################################\n");


   if (layer == -1)
   {
      for (int i=0;i<nr_of_items;i++)
      {
         x = the_list[i].x - playfield->vp_x;
         y = the_list[i].y - playfield->vp_y;
         if ( x < 0 || x > playfield->vp_w || y < 0 || y > playfield->vp_h)
            continue;
         rectfill(onto, x * TILESIZE_X, y * TILESIZE_Y,x * TILESIZE_X + TILESIZE_X - 1 , y * TILESIZE_Y  + TILESIZE_Y - 1, 0);
      }
   }
   else
   {
      for (int i=0;i<nr_of_items;i++)
      {
          playfield->draw(onto, the_list[i].x,the_list[i].y,layer);
      }
   }
}


void DRAWLIST::for_all(void (*proc)(int x , int y))
{
     int x,y;
     for (int i=0;i<nr_of_items;i++)
     {
         x = the_list[i].x - playfield->vp_x;
         y = the_list[i].y - playfield->vp_y;
         
         if (x>=0 && y>=0 && x <= playfield->vp_w && y <= playfield->vp_h)
                  proc(x ,y);
     }
}


/*HEAD class: dummy object for head of functionlist */


HEAD::HEAD()
{
}

HEAD::~HEAD()
{
   OBJECT *tmp;
   while (get_next())
   {
      tmp = get_next();
      get_next()->remove_from_list();
      delete tmp;
   }
}

int HEAD::proc()
{
   return 0;
}

void HEAD::draw(BITMAP *onto, int pos_x, int pos_y, int layer)
{
   pos_x =pos_y = layer = 0;
   onto = 0; // suppress warning unused
   return;
}

FUNCTIONLIST::FUNCTIONLIST()
{
//   _MM_MARK();
   head = new HEAD();
//   _MM_MARK();
   waitroom = new HEAD();
//   _MM_MARK();
   if (!head)
     out_of_mem("creating functionlist");
 //  _MM_MARK();
}


FUNCTIONLIST::~FUNCTIONLIST()
{
   while (head->get_next())
         head->get_next()->remove_from_list();

   delete head;
   delete waitroom;
}

void FUNCTIONLIST::do_procs()
{
   int ret;
   OBJECT *tmp;
   // insert waitrooom in head

//   fprintf(stderr,"do_procs*****************************************\n");
   if (waitroom->next)
   {
      // search for the last one
      tmp = waitroom->next;
      
      while (tmp->next)
            tmp = tmp->next;
            

      tmp->next = head->next;
      if (head->next)
            head->next->last = tmp;
            
      
      head->next = waitroom->next;
      waitroom->next->last = head;
      
      waitroom->next = NULL;
   }

   for (OBJECT *i = head; i->get_next(); i = i->get_next())
   {

      while (i->get_next()->must_be_deleted)
      {
            tmp = i->get_next();
            tmp->must_be_deleted = FALSE;
            // if the map contains a pointer to this  object, remove it
            playfield->del(tmp->mapx, tmp->mapy,tmp);
            tmp->remove_from_list();
            
            if (!tmp->nodelete)
               objcol->del(tmp);
               
            if (!i->get_next())
                return;            
      }

//      fprintf(stderr,"do %s\n",i->get_next()->name);
      ret = i->get_next()->proc();
//      fprintf(stderr,"did %s %d frame %d\n",i->get_next()->name, i->get_next()->id, i->get_next()->frame);
      drawlist->add(i->get_next());

      switch (ret)
      {
        case F_DONE:
            i->get_next()->remove_from_list();
            // now check if the is still a next one
            
            if (!i->get_next())
                return;            
                
            break;
        case F_DELETE:
            tmp = i->get_next();
            // if the map contains a pointer to this  object, remove it
            playfield->del(tmp->mapx, tmp->mapy,tmp);
            tmp->remove_from_list();

            if (!tmp->nodelete)
               objcol->del(tmp);


            if (!i->get_next())
                return;            
            break;
        default:
            // fall through intended
        case F_CONTINUE:
            break;
      }
   }
}

void FUNCTIONLIST::add(OBJECT *obj)
{
   obj->add_to_list(waitroom);
}

Score::Score()
{
   houws = poorts= energy = points = lives =stars = 0;

}


