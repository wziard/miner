#include "globals.h"
#include "gfxdata.h"
#include "schedel.h"
#include "objidx.h"

SCHEDEL::SCHEDEL()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_SCHEDEL].dat);
   tag = TAG_MONSTER;
   passflags |= PF_PLAYER;
   score.lives = -1;
   name ="SCHEDEL";
   idx = OBJ_SCHEDEL;
}

int SCHEDEL::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


