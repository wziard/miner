#include "globals.h"
#include "gfxdata.h"
#include "objidx.h"
#include "rtile.h"

RANDTILE::RANDTILE()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_RAND].dat);
   tag = TAG_BACKGROUND;
   name = "RANDTILE";
   idx = OBJ_RTILE;
}



int RANDTILE::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


