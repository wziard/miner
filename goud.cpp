#include "globals.h"
#include "gfxdata.h"
#include "goud.h"
#include "objidx.h"

GOUD::GOUD()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_GOUD].dat);
   tag = TAG_BONUS;
   passflags |= PF_PLAYER;
   score.points = 10;
   name ="GOUD";
   idx = OBJ_GOUD;
}

int GOUD::proc()
{
   return F_DONE; // exit from proclist immediately (don't need to do anything)
}


