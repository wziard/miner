#ifndef _OBJECTS_H_
#define _OBJECTS_H_
#include "list.h"



// this object contains just one sprite
class STATIC_OBJECT
      :public OBJECT
{
   public:
      STATIC_OBJECT();
      void draw(BITMAP *onto, int x, int y, int layer);
      
      RLE_SPRITE *sprite;
};


typedef struct ANIM
{
 BITMAP **frames;
 int last_frame;
 
} ANIM;


class GFX_OBJECT
{
   public:
    GFX_OBJECT(int nr_of_states);
    ~GFX_OBJECT();
    void set_state_anim(int _state, BITMAP *strip);

    void draw(BITMAP *onto,int x, int y, int state, int frame);
   private:
    int nr_of_states;
    ANIM *states;
};


class GFX_OBJECTS_TABLE
{
   public:
     GFX_OBJECTS_TABLE(int nr_of_objects);
     ~GFX_OBJECTS_TABLE();
     void init_object(int obj,int nr_of_states);
     void set_object_state(int object, int state, BITMAP *strip);
     void draw(BITMAP *onto,int x, int y, int object, int state, int frame);
     GFX_OBJECT **gfx_objects;
   private:
     int nr;

};


// this object contains an animation
class ANIMATED_OBJECT
      :public OBJECT
{
   public:
      ANIMATED_OBJECT(BITMAP *_sprites);
      ~ANIMATED_OBJECT();

      void draw(BITMAP *onto, int x, int y, int layer);
   protected:
      static int instances;
      static BITMAP *tmp_sprite;
      BITMAP *sprites;
      int nr_of_frames;
};




// this object is fully animated and contains a set of animations
class COMPLEX_OBJECT
      :public OBJECT
{
   public:
      COMPLEX_OBJECT();
      ~COMPLEX_OBJECT();
      void draw(BITMAP *onto, int x, int y, int layer);
   protected:
      GFX_OBJECT *gfx_object;
      int my_layer;
      int nr_of_anims;
};

class PLACEHOLDER
    : public OBJECT
{
   public:
      PLACEHOLDER();
      int proc();
      void draw(BITMAP *onto, int x, int y, int layer);
      int can_coexist(int tag);
};



#endif
