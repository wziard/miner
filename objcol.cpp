#include "objcol.h"
#include "helpers.h"
#include "objidx.h"
#include "snd.h"

OBJECT_COLLECTION::OBJECT_COLLECTION()
{
   collection = new HEAD[NR_OF_OBJECTS];
}

OBJECT_COLLECTION::~OBJECT_COLLECTION()
{
   delete [] collection;
}





OBJECT *OBJECT_COLLECTION::new_obj(int idx, int extra, int extra2)
{
   OBJECT *ret;

   if (idx == OBJ_NET && !extra &&! extra2)
   {
      extra = NET_STATE_STAND;
      extra2 = 10;
   }
   if (collection[idx].next)
   {
      ret = collection[idx].next;
      ret->remove_from_list();
//      fprintf(stderr,"re-using %s %d from %d\n",ret->name, ret->id, idx);
//      if (!(collection[idx].next))
//         fprintf(stderr,"collection empty\n");
      
   }
   else
   {
      ret = really_new(idx, extra, extra2);
//      fprintf(stderr,"created new %s %d\n",ret->name, ret->id);
   }
     
   ret->set_state(extra,extra2);
//   fprintf(stderr,"   frame = %d\n",ret->frame);
   return ret;

}


void OBJECT_COLLECTION::del(OBJECT *obj)
{
   obj->remove_from_list();
   obj->add_to_list(collection + obj->idx);
   kill_loop(&(obj->voice));
   
//   fprintf(stderr,"storing  %s %d at %d  frame %d\n",obj->name,obj->id, obj->idx, obj->frame);

//   if (obj->idx == OBJ_PLACEHOLDER)
//      rectfill (NULL,0,0,10,10,10);
}


OBJECT *OBJECT_COLLECTION::really_new(int idx, int extra, int extra2)
{
   OBJECT *ret;
   switch(idx)
   {
      case   OBJ_PLACEHOLDER:
       ret = new PLACEHOLDER;
       break;
      case   OBJ_BOX:
       ret = new BOX;
       break;
      case   OBJ_BALLOON:
       ret = new BALLOON;
       break;
      case   OBJ_LINKS:
       ret = new LINKS;
       break;
      case   OBJ_RECHTS:
       ret = new RECHTS;
       break;
      case   OBJ_MINE:
       ret = new MINE;
       break;
      case   OBJ_BRICK:
       ret = new BRICK;
       break;
      case   OBJ_BRICK2:
       ret = new BRICK2;
       break;
      case   OBJ_BRICK3:
       ret = new BRICK3;
       break;
      case   OBJ_BACKG:
       ret = new BACKGROUND;
       break;
      case   OBJ_BACKG2:
       ret = new BACKGROUND2;
       break;
      case   OBJ_BACKG3:
       ret = new BACKGROUND3;
       break;
      case   OBJ_FLAME:
       ret = new FLAME;
       break;
      case   OBJ_NE_RE:
       ret = new NE_RE;
       break;
      case   OBJ_STER:
       ret = new STER;
       break;
      case   OBJ_GOUD:
       ret = new GOUD;
       break;
      case   OBJ_PSTUT:
       ret = new PSTUT;
       break;
      case   OBJ_CROWN :
       ret = new CROWN;
       break;
      case   OBJ_SCHEDEL:
       ret = new SCHEDEL;
       break;
      case   OBJ_BOEM:
       ret = new BOEM;
       break;
      case   OBJ_BOMB:
       ret = new BOMB;
       break;
      case   OBJ_BREAK:
       ret = new BREAK;
       break;
      case   OBJ_NE_LI:
       ret = new NE_LI;
       break;
      case   OBJ_HEKOP:
       ret = new HEKOP;
       break;
      case   OBJ_HEKNEER:
       ret = new HEKNEER;
       break;
      case   OBJ_BLOKAL:
       ret = new BLOKAL;
       break;
      case   OBJ_GROEN:
       ret = new GROEN(extra);
       break;
       
       case   OBJ_ENERG:
       ret = new ENERG;
       break;
      case   OBJ_HOUWEEL:
       ret = new HOUWEEL;
       break;
      case   OBJ_BOMAL:
       ret = new BOMAL;
       break;
      case   OBJ_NET:
       ret = new NET(extra, extra2);
       break;
      case   OBJ_STUT:
       ret = new STUT;
       break;
      case   OBJ_MAN:
       ret = new MAN(extra2);
       break;
      case   OBJ_RTILE:
       ret = new RANDTILE;
       break;
      case   OBJ_BURN:
       ret = new BURN(extra);
       break;
      case   OBJ_MONSTER:
       ret = new MONSTER;
       break;

      default:
              ret = NULL;
              printerror("unknown object idx asked: %d",idx);
              break;
   }
   return ret;
}
