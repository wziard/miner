#include "globals.h"
#include "data.h"
#include "blokal.h"
#include "setje.h"
#include "objidx.h"

BLOKAL::BLOKAL()
{
   sprite = (RLE_SPRITE *)(objects[GFXDATA_OBJECTS_BLOKAL].dat);
   tag = TAG_MONSTER;
   passflags = PF_AXER;
   score.points = 0;
   state = BLOKAL_STATE_STANDING;
   speed = 1;
   name = "BLOKAL";
   idx = OBJ_BLOKAL;
}

int BLOKAL::proc()
{
   frame++;
   frame %= (32 / speed);
   switch(state)
   {

      case BLOKAL_STATE_FALLING:
        if (frame == (32 / speed) - 1)
        {
            move(0,1);

            if (!playfield->allowed_move(mapx ,mapy+1,PF_MONSTER))
            {
               playfield->set (mapx,mapy, objcol->new_obj(get_random_object (1)));
               state = BLOKAL_STATE_STANDING;
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs+=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;

        break;

      case BLOKAL_STATE_RISING:
        if (frame == (32 / speed) - 1)
        {
            move(0,-1);
            if (!playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
            {
               playfield->set (mapx,mapy, objcol->new_obj(get_random_object (1)));
               state = BLOKAL_STATE_STANDING;
               return F_DELETE;
            }
               
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            yoffs-=speed;
        }
        return F_CONTINUE;
        break;

      case BLOKAL_STATE_LINKS:
        if (frame == (32 / speed) - 1)
        {
            move(-1,0);

            if (!playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
            {
               playfield->set (mapx,mapy, objcol->new_obj(get_random_object (1)));
               state = BLOKAL_STATE_STANDING;
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            }
        }
        else
        {
            xoffs-=speed;
        }
        drawlist->add(this);
        return F_CONTINUE;
        break;

      case BLOKAL_STATE_RECHTS:
        if (frame == (32 / speed) - 1)
        {
            move(1,0);

            if (!playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
            {
               playfield->set (mapx,mapy, objcol->new_obj(get_random_object (1)));
               state = BLOKAL_STATE_STANDING;
               return F_DELETE;
            }
            else
            {
//               speed = MIN(speed<<1, 8);
               playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
               
            }
        }
        else
        {
            xoffs+=speed;
        }
        return F_CONTINUE;
        break;


      default:
        if (playfield->allowed_move(mapx-1,mapy,PF_MONSTER))
        {
            state = BLOKAL_STATE_LINKS;
            frame = 0;
            speed = 1;
            playfield->set(mapx-1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->allowed_move(mapx+1,mapy,PF_MONSTER))
        {
            state = BLOKAL_STATE_RECHTS;
            frame = 0;
            speed = 1;
            playfield->set(mapx+1,mapy,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
            
        }
        if (playfield->allowed_move(mapx,mapy+1,PF_MONSTER))
        {
            state = BLOKAL_STATE_FALLING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy+1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        if (playfield->allowed_move(mapx,mapy-1,PF_MONSTER))
        {
            state = BLOKAL_STATE_RISING;
            frame = 0;
            speed = 1;
            playfield->set(mapx,mapy-1,objcol->new_obj(OBJ_PLACEHOLDER));
            return F_CONTINUE;
        }
        break;
   }
   return F_DONE;
}
